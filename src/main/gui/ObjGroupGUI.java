package main.gui;

import main.Parser;
import main.dao.Component;
import main.dao.Enterprise;
import main.dao.ObjGroup;
import main.dao.Subsystem;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ObjGroupGUI {

    private JFrame frame;
    private JPanel panelEditFields;
    private JPanel panelListObjGroups;
    private JPanel panelListComp;
    private JPanel panelCompOfObjGrp;
    private JPanel panelButtom;
    private JPanel panelChangeDelete;
    private JLabel nameLabel;
    private JTextField nameEdt;
    private JLabel commentLabel;
    private JTextField commentEdt;
    private JButton buttonAdd;
    private JButton buttonChange;
    private JButton buttonDelete;
    private JButton buttonForAdd;
    private JList listComponents;
    private JList listObjGroup;
    private JList listCompsOfObjGrp;
    private JLabel messageLabel;
    private JButton removeEdge;

    private List<Component> componentsList;
    private List<ObjGroup> objGroupsList;
    private List<Component> currComponentsList;


    private abstract class ThreadObjGroup extends Thread {
        protected ObjGroup objGroup;

        public void setObjGroup(ObjGroup objGroup) {
            this.objGroup = objGroup;
        }
    }

    private class ThreadChange extends ThreadObjGroup {
        @Override
        public void run() {
            Parser.changeObjGroup(objGroup,  currComponentsList.size() == 0 ? null : currComponentsList.get(0));
        }
    }

    private class ThreadDelete extends ThreadObjGroup {
        @Override
        public void run() {
            Parser.deleteObjGroup(objGroup);
        }
    }

    public void initGUI(){
        frame = new JFrame("ObjectGroup");
        frame.setSize(900, 600);
        frame.setResizable(false);
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLayout(new BorderLayout());

        initLeftPanel(frame);
        initRightPanel(frame);
        initCentrePanel(frame);
        initBottomPanel(frame);
    }

    protected void initLeftPanel(JFrame frame) {
        panelListObjGroups = new JPanel();
        panelListObjGroups.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 10));
        panelListObjGroups.setBackground(new Color(231, 226, 253));
        panelListObjGroups.setPreferredSize(new Dimension(200, 500));

        objGroupsList = Parser.getAllObjGroups();
//        listObjGroup = new JList(objGroupsList.toArray());
//        listObjGroup.setPreferredSize(new Dimension(200, 400));

        JScrollPane scrollPane = new JScrollPane();
        listObjGroup = new JList(objGroupsList.toArray());
        scrollPane.setViewportView(listObjGroup);
        scrollPane.setPreferredSize(new Dimension(200, 400));


        listObjGroup.addListSelectionListener(e -> {

        });

        panelListObjGroups.add(new JLabel("ObjectGroup"));
        panelListObjGroups.add(scrollPane);
        JButton btnSelect = new JButton("Выбрать");
        btnSelect.addActionListener(e->{
            int index = listObjGroup.getSelectedIndex();
            if (index >= 0) {
                ObjGroup selectedObjGroup = objGroupsList.get(index);
                nameEdt.setText(selectedObjGroup.getObjgname());
                commentEdt.setText(selectedObjGroup.getBelongsto());
                buttonAdd.setVisible(false);
                buttonChange.setVisible(true);
                buttonDelete.setVisible(true);
                initObjectGroupsByComponent(selectedObjGroup);
            }
        });
        panelListObjGroups.add(btnSelect);
        buttonForAdd = new JButton("Добавить новый");
        panelListObjGroups.add(buttonForAdd);
        buttonForAdd.addActionListener(e -> {
            buttonAdd.setVisible(true);
            buttonChange.setVisible(false);
            buttonDelete.setVisible(false);
            clearAll();
        });

        frame.getContentPane().add(panelListObjGroups, BorderLayout.WEST);
    }


    protected void initCentrePanel(JFrame frame) {
        panelEditFields = new JPanel();
        panelEditFields.setBackground(new Color(231, 226, 253));
        panelEditFields.setPreferredSize(new Dimension(500, 500));
        panelEditFields.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 10));

        nameLabel = new JLabel("Name");
        nameLabel.setPreferredSize(new Dimension(400, 20));

        nameEdt = new JTextField();
        nameEdt.setPreferredSize(new Dimension(400, 30));

        commentLabel = new JLabel("Belongsto");
        commentLabel.setPreferredSize(new Dimension(400, 20));

        commentEdt = new JTextField();
        commentEdt.setPreferredSize(new Dimension(400, 30));

        buttonAdd = new JButton("Добавить");
        buttonAdd.setPreferredSize(new Dimension(300, 30));

        buttonChange = new JButton("Изменить");
        buttonChange.setPreferredSize(new Dimension(150, 30));
        buttonChange.setVisible(false);

        buttonDelete = new JButton("Удалить");
        buttonDelete.setPreferredSize(new Dimension(150, 30));
        buttonDelete.setVisible(false);

        panelCompOfObjGrp = new JPanel();
        panelCompOfObjGrp.setPreferredSize(new Dimension(400, 100));
        panelCompOfObjGrp.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));

        panelChangeDelete = new JPanel();
        panelChangeDelete.setPreferredSize(new Dimension(400, 30));
        panelChangeDelete.setBackground(new Color(231, 226, 253));
        panelChangeDelete.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));

        panelChangeDelete.add(buttonAdd);
        panelChangeDelete.add(buttonChange);
        panelChangeDelete.add(buttonDelete);


        buttonAdd.addActionListener(e -> {
            String strName = nameEdt.getText();
            String strComment = commentEdt.getText();

            if (!strComment.isEmpty() && !strName.isEmpty()) {
                ObjGroup objGroup = new ObjGroup(strName, strComment);
                if (currComponentsList != null) {
                    ObjGroup obGr = Parser.addObjGroup(objGroup, currComponentsList.get(0));
                    if (obGr != null) {
                        messageLabel.setText("Добавленно");
                        clearAll();
                        objGroupsList.add(obGr);

                        DefaultListModel listModel = new DefaultListModel();
                        for (ObjGroup item : objGroupsList) {
                            listModel.addElement(item.toString());
                        }
                        listObjGroup.setModel(listModel);

                    } else {
                        messageLabel.setText("Ошибка при добавленно");
                    }
                } else {
                    messageLabel.setText("Нужно добавить хотя бы один Enterprise!");
                }

            }
        });


        buttonChange.addActionListener(e -> {
            int index = listObjGroup.getSelectedIndex();

            String strName = nameEdt.getText();
            String strComment = commentEdt.getText();

            if (!strComment.isEmpty() && !strName.isEmpty()) {
                if (index >= 0) {
                    ObjGroup objGroup = objGroupsList.get(index);
                    if (!strComment.isEmpty()) objGroup.setBelongsto(strComment);
                    if (!strName.isEmpty()) objGroup.setObjgname(strName);

                    ThreadObjGroup myThread = new ThreadChange();
                    myThread.setObjGroup(objGroup);
                    myThread.start();

                    messageLabel.setText("Сохраннено");
                    clearAll();
                }
            }
        });

        buttonDelete.addActionListener(e -> {
            int index = listObjGroup.getSelectedIndex();

            if (index >= 0) {
                ObjGroup objGroup = objGroupsList.get(index);

                int dialogResult = JOptionPane.showConfirmDialog (null, "Вы действительно хотите удалить данную ObjGroup запись?");
                if(dialogResult == JOptionPane.YES_OPTION){

                    ThreadObjGroup myThread = new ThreadDelete();
                    myThread.setObjGroup(objGroup);
                    myThread.start();

                    messageLabel.setText("Удаленно");
                    objGroupsList.remove(index);
                    clearAll();
                    DefaultListModel listModel = new DefaultListModel();
                    for (ObjGroup item : objGroupsList) {
                        listModel.addElement(item.toString());
                    }
                    listObjGroup.setModel(listModel);
                }
            }

        });

//        JLabel labelSubs = new JLabel("Components");
//        labelSubs.setPreferredSize(new Dimension(400 , 30));
        JPanel panelRemoveEdges = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));
        panelRemoveEdges.setPreferredSize(new Dimension(400 , 30));
        panelRemoveEdges.setBackground(new Color(231, 226, 253));
        JLabel labelSubs = new JLabel("Components-ObjGroups");
        panelRemoveEdges.add(labelSubs);

        removeEdge = new JButton("Убрать Subsystem");
        removeEdge.setVisible(false);
        removeEdge.addActionListener(e -> {

            if (listCompsOfObjGrp != null) {
                int index = listCompsOfObjGrp.getSelectedIndex();
                currComponentsList.remove(index);

                if (index != -1) {
                    DefaultListModel listModel = new DefaultListModel();
                    for (Component en : currComponentsList) {
                        listModel.addElement(en.toString());
                    }

                    listCompsOfObjGrp.setModel(listModel);
                }
            }
        });
        panelRemoveEdges.add(removeEdge);

        panelEditFields.add(nameLabel);
        panelEditFields.add(nameEdt);
        panelEditFields.add(commentLabel);
        panelEditFields.add(commentEdt);
        panelEditFields.add(panelRemoveEdges);
        panelEditFields.add(panelCompOfObjGrp);
        panelEditFields.add(panelChangeDelete);
        frame.getContentPane().add(panelEditFields, BorderLayout.CENTER);
    }

    protected void initRightPanel(JFrame frame) {
        panelListComp = new JPanel();
        panelListComp.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 10));
        panelListComp.setBackground(new Color(231, 226, 253));
        panelListComp.setPreferredSize(new Dimension(200, 500));

        componentsList = Parser.getAllComponents();
//        listComponents = new JList(componentsList.toArray());
//        listComponents.setPreferredSize(new Dimension(200, 400));

        JScrollPane scrollPane = new JScrollPane();
        listComponents = new JList(componentsList.toArray());
        scrollPane.setViewportView(listComponents);
        scrollPane.setPreferredSize(new Dimension(200, 400));



        panelListComp.add(new JLabel("Components"));
        panelListComp.add(scrollPane);

        JButton button = new JButton("Добавить к ObjectGroup");
        panelListComp.add(button);
        button.addActionListener(e->{
            int index = listComponents.getSelectedIndex();
            Component currComp = componentsList.get(index);

            currComponentsList = new ArrayList<>();

            for (Component c : currComponentsList) {
                if (c.getId() == currComp.getId()){
                    return;
                }
            }

            currComponentsList.add(currComp);

            DefaultListModel listModel = new DefaultListModel();
            for (Component c : currComponentsList) {
                listModel.addElement(c.toString());
            }

            if (listCompsOfObjGrp != null) {
                listCompsOfObjGrp.setModel(listModel);
            } else {
                listCompsOfObjGrp = new JList(listModel);
                listCompsOfObjGrp.setPreferredSize(new Dimension(400, 100));
                panelCompOfObjGrp.add(listCompsOfObjGrp);
            }

            listCompsOfObjGrp.revalidate();
            listCompsOfObjGrp.repaint();
        });
        frame.getContentPane().add(panelListComp, BorderLayout.EAST);
    }




    protected void initBottomPanel (JFrame frame) {
        panelButtom = new JPanel();
        panelButtom.setLayout(new FlowLayout());
        panelButtom.setBackground(new Color(231, 226, 253));
        panelButtom.setPreferredSize(new Dimension(700, 50));

        messageLabel = new JLabel();
        panelButtom.add(messageLabel);
        frame.add(panelButtom, BorderLayout.SOUTH);


    }

    protected void clearAll() {
        removeEdge.setVisible(false);
        listObjGroup.clearSelection();
        nameEdt.setText("");
        commentEdt.setText("");
        initObjectGroupsByComponent(null);
    }

    protected void initObjectGroupsByComponent (ObjGroup objGroup) {

        panelCompOfObjGrp.removeAll();
        if (currComponentsList != null) currComponentsList.clear();


        if (objGroup != null) {
            currComponentsList = Parser.getComponentByObjGroups(objGroup);
            listCompsOfObjGrp = new JList(currComponentsList.toArray());
        } else {
            listCompsOfObjGrp = new JList(new String[1]);
        }

        listCompsOfObjGrp.setPreferredSize(new Dimension(400, 100));
        panelCompOfObjGrp.add(listCompsOfObjGrp);


        listCompsOfObjGrp.addListSelectionListener(e -> {
            removeEdge.setVisible(true);
            /**
             * @TODO delete subsystems?!!!
             */
        });

        panelCompOfObjGrp.revalidate();
        panelCompOfObjGrp.repaint();
    }
}
