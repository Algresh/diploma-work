package main.gui;

import main.dao.Enterprise;

import javax.swing.*;
import java.awt.*;

public class MainGUI {

    private JFrame frame;

    public static void main(String[] args) {
        MainGUI mainGUI = new MainGUI();
        mainGUI.initGUI();

    }

    public void initGUI(){
        frame = new MyFrame("Enterprise");
        frame.setSize(850, 700);
        frame.setVisible(true);
        frame.setLayout(new BorderLayout());
        frame.setBackground(new Color(231, 226, 253));
//        frame.setResizable(false);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 50));
        mainPanel.setPreferredSize(new Dimension(500, 700));
        mainPanel.setBackground(new Color(231, 226, 253));
//        mainPanel.setBackground(Color.YELLOW);

        JPanel leftPanel = new JPanel();
        leftPanel.setPreferredSize(new Dimension(200, 700));
        leftPanel.setBackground(new Color(231, 226, 253));

        JButton btnSetting = new JButton("Настройки");
        btnSetting.setPreferredSize(new Dimension(150, 30));
        btnSetting.setBorder(null);
        btnSetting.setBackground(new Color(133, 228, 150));

        leftPanel.add(btnSetting);

        btnSetting.addActionListener(e -> {
            SettingGUI settingGUI = new SettingGUI();
            settingGUI.initGUI();
        });

        JPanel rightPanel = new JPanel();
        rightPanel.setPreferredSize(new Dimension(200, 700));
        rightPanel.setBackground(new Color(231, 226, 253));

        JButton btnEnter1 = new JButton("Enterprise");
        btnEnter1.setPreferredSize(new Dimension(400, 50));
        mainPanel.add(btnEnter1);
        btnEnter1.addActionListener(e -> {
            EnterpriseGUI enterpriseGUI = new EnterpriseGUI();
            enterpriseGUI.initGUI();
        });
        btnEnter1.setBorder(null);
        btnEnter1.setBackground(new Color(133, 228, 150));

        JButton btnEnter2 = new JButton("Subsystem");
        btnEnter2.setPreferredSize(new Dimension(400, 50));
        mainPanel.add(btnEnter2);
        btnEnter2.addActionListener(e -> {
            SubsystemGUI subsystemGUI = new SubsystemGUI();
            subsystemGUI.initGUI();
        });

        btnEnter2.setBorder(null);
        btnEnter2.setBackground(new Color(133, 228, 150));

        JButton btnEnter3 = new JButton("Component");
        btnEnter3.setPreferredSize(new Dimension(400, 50));
        mainPanel.add(btnEnter3);
        btnEnter3.addActionListener(e -> {
            ComponentGUI componentGUI = new ComponentGUI();
            componentGUI.initGUI();
        });

        btnEnter3.setBorder(null);
        btnEnter3.setBackground(new Color(133, 228, 150));

        JButton btnEnter4 = new JButton("Object Group");
        btnEnter4.setPreferredSize(new Dimension(400, 50));
        mainPanel.add(btnEnter4);
        btnEnter4.addActionListener(e -> {
            ObjGroupGUI objGroupGUI = new ObjGroupGUI();
            objGroupGUI.initGUI();
        });

        btnEnter4.setBorder(null);
        btnEnter4.setBackground(new Color(133, 228, 150));

        JButton btnEnter5 = new JButton("Object");
        btnEnter5.setPreferredSize(new Dimension(400, 50));
        mainPanel.add(btnEnter5);
        btnEnter5.addActionListener(e -> {
            ObjectGUI objectGUI = new ObjectGUI();
            objectGUI.initGUI();
        });

        btnEnter5.setBorder(null);
        btnEnter5.setBackground(new Color(133, 228, 150));

        JButton btnEnter6 = new JButton("Incident");
        btnEnter6.setPreferredSize(new Dimension(400, 50));
        mainPanel.add(btnEnter6);
        btnEnter6.addActionListener(e -> {
            IncidentGUI incidentGUI = new IncidentGUI();
            incidentGUI.initGUI();
        });

        btnEnter6.setBorder(null);
        btnEnter6.setBackground(new Color(133, 228, 150));

        frame.add(mainPanel, BorderLayout.CENTER);
        frame.add(leftPanel, BorderLayout.WEST);
        frame.add(rightPanel, BorderLayout.EAST);
    }
}
