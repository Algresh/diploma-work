package main.gui;


import main.Parser;
import main.dao.Enterprise;
import main.dao.Subsystem;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class SubsystemGUI {

    private JFrame frame;
    private JPanel panelEditFields;
    private JPanel panelListSubsys;
    private JPanel panelListEnter;
    private JPanel panelEnterOfSub;
    private JPanel panelButtom;
    private JPanel panelChangeDelete;
    private JLabel nameLabel;
    private JTextField nameEdt;
    private JLabel commentLabel;
    private JTextField commentEdt;
    private JButton buttonAdd;
    private JButton buttonChange;
    private JButton buttonDelete;
    private JButton buttonForAdd;
    private JList listEnterprises;
    private JList listSubsystem;
    private JList listEntrsOfSub;
    private JLabel messageLabel;
    private JButton removeEdge;

    private List<Enterprise> enterpriseList;
    private List<Subsystem> subsystemList;
    private List<Enterprise> currEnterprisesList;

    private abstract class ThreadSubsystem extends Thread {
        protected Subsystem subsystem;

        public void setSubsystem(Subsystem subsystem) {
            this.subsystem = subsystem;
        }
    }

    private class ThreadChange extends ThreadSubsystem {
        @Override
        public void run() {
            Parser.changeSubsystem(subsystem, currEnterprisesList);
        }
    }

    private class ThreadDelete extends ThreadSubsystem {
        @Override
        public void run() {
            Parser.deleteSubsystem(subsystem);
        }
    }


    public void initGUI(){
        frame = new JFrame("Subsystems");
        frame.setSize(900, 600);
        frame.setResizable(false);
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLayout(new BorderLayout());

        initLeftPanel(frame);
        initRightPanel(frame);
        initCentrePanel(frame);
        initBottomPanel(frame);
    }


    protected void initLeftPanel(JFrame frame) {
        panelListSubsys = new JPanel();
        panelListSubsys.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 10));
        panelListSubsys.setBackground(new Color(231, 226, 253));
        panelListSubsys.setPreferredSize(new Dimension(200, 500));

        subsystemList = Parser.getAllSubsystems();
//        listSubsystem = new JList(subsystemList.toArray());
//        listSubsystem.setPreferredSize(new Dimension(200, 400));

        JScrollPane scrollPane = new JScrollPane();
        listSubsystem = new JList(subsystemList.toArray());
        scrollPane.setViewportView(listSubsystem);
        scrollPane.setPreferredSize(new Dimension(200, 400));

        listSubsystem.addListSelectionListener(e -> {

        });

        panelListSubsys.add(new JLabel("Subsystems"));
        panelListSubsys.add(scrollPane);
        JButton btnSelect = new JButton("Выбрать");
        btnSelect.addActionListener(e -> {
            int index = listSubsystem.getSelectedIndex();
            if (index >= 0) {
                Subsystem selectedSubsys = subsystemList.get(index);
                nameEdt.setText(selectedSubsys.getSubsystemName());
                commentEdt.setText(selectedSubsys.getComment());
                buttonAdd.setVisible(false);
                buttonChange.setVisible(true);
                buttonDelete.setVisible(true);
                initEnterprisesBySubsystem(selectedSubsys);
            }
        });
        panelListSubsys.add(btnSelect);
        buttonForAdd = new JButton("Добавить новый");
        panelListSubsys.add(buttonForAdd);
        buttonForAdd.addActionListener(e -> {
            buttonAdd.setVisible(true);
            buttonChange.setVisible(false);
            buttonDelete.setVisible(false);
            clearAll();
        });

        frame.getContentPane().add(panelListSubsys, BorderLayout.WEST);
    }

    protected void initCentrePanel(JFrame frame) {
        panelEditFields = new JPanel();
        panelEditFields.setBackground(new Color(231, 226, 253));
        panelEditFields.setPreferredSize(new Dimension(500, 500));
        panelEditFields.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 10));

        nameLabel = new JLabel("Name");
        nameLabel.setPreferredSize(new Dimension(400, 20));

        nameEdt = new JTextField();
        nameEdt.setPreferredSize(new Dimension(400, 30));

        commentLabel = new JLabel("Comment");
        commentLabel.setPreferredSize(new Dimension(400, 20));

        commentEdt = new JTextField();
        commentEdt.setPreferredSize(new Dimension(400, 30));

        buttonAdd = new JButton("Добавить");
        buttonAdd.setPreferredSize(new Dimension(300, 30));

        buttonChange = new JButton("Изменить");
        buttonChange.setPreferredSize(new Dimension(150, 30));
        buttonChange.setVisible(false);

        buttonDelete = new JButton("Удалить");
        buttonDelete.setPreferredSize(new Dimension(150, 30));
        buttonDelete.setVisible(false);

        panelEnterOfSub = new JPanel();
        panelEnterOfSub.setPreferredSize(new Dimension(400, 100));
        panelEnterOfSub.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));

        panelChangeDelete = new JPanel();
        panelChangeDelete.setPreferredSize(new Dimension(400, 30));
        panelChangeDelete.setBackground(new Color(231, 226, 253));
        panelChangeDelete.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));

        panelChangeDelete.add(buttonAdd);
        panelChangeDelete.add(buttonChange);
        panelChangeDelete.add(buttonDelete);


        buttonAdd.addActionListener(e -> {
            String strName = nameEdt.getText();
            String strComment = commentEdt.getText();

            if (!strComment.isEmpty() && !strName.isEmpty()) {
                Subsystem subsystem = new Subsystem(strName, strComment);
                if (currEnterprisesList != null) {
                    Subsystem sub = Parser.addSubsystem(subsystem, currEnterprisesList);
                    if (sub != null) {
                        messageLabel.setText("Добавленно");
                        clearAll();
                        subsystemList.add(sub);

                        DefaultListModel listModel = new DefaultListModel();
                        for (Subsystem item : subsystemList) {
                            listModel.addElement(item.toString());
                        }
                        listSubsystem.setModel(listModel);

                    } else {
                        messageLabel.setText("Ошибка при добавленно");
                    }
                } else {
                    messageLabel.setText("Нужно добавить хотя бы один Enterprise!");
                }

            }
        });


        buttonChange.addActionListener(e -> {
            int index = listSubsystem.getSelectedIndex();

            String strName = nameEdt.getText();
            String strComment = commentEdt.getText();

            if (!strComment.isEmpty() && !strName.isEmpty()) {
                if (index >= 0) {
                    Subsystem subsystem = subsystemList.get(index);
                    if (!strComment.isEmpty()) subsystem.setComment(strComment);
                    if (!strName.isEmpty()) subsystem.setSubsystemName(strName);

//                    System.out.print(Parser.changeSubsystem(subsystem, currEnterprisesList));
                    ThreadSubsystem myThread = new ThreadChange();
                    myThread.setSubsystem(subsystem);
                    myThread.start();

                    messageLabel.setText("Сохраннено");
                    clearAll();
                }
            }
        });

        buttonDelete.addActionListener(e -> {
            int index = listSubsystem.getSelectedIndex();

            if (index >= 0) {
                Subsystem subsystem = subsystemList.get(index);

                int dialogResult = JOptionPane.showConfirmDialog (null, "Вы действительно хотите удалить данную Subsystem запись?");
                if(dialogResult == JOptionPane.YES_OPTION){


                    ThreadSubsystem myThread = new ThreadDelete();
                    myThread.setSubsystem(subsystem);
                    myThread.start();

                    messageLabel.setText("Удаленно");
                    subsystemList.remove(index);
                    clearAll();
                    DefaultListModel listModel = new DefaultListModel();
                    for (Subsystem item : subsystemList) {
                        listModel.addElement(item.toString());
                    }
                    listSubsystem.setModel(listModel);
                }
            }

        });


//        labelSubs.setPreferredSize(new Dimension(400 , 30));

        JPanel panelRemoveEdges = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));
        panelRemoveEdges.setPreferredSize(new Dimension(400 , 30));
        panelRemoveEdges.setBackground(new Color(231, 226, 253));
        JLabel labelSubs = new JLabel("Enterprises-Subsystems");
        panelRemoveEdges.add(labelSubs);

        removeEdge = new JButton("Убрать Enterprise");
        removeEdge.setVisible(false);
        removeEdge.addActionListener(e -> {

            if (listEntrsOfSub != null) {
                int index = listEntrsOfSub.getSelectedIndex();
                currEnterprisesList.remove(index);

                if (index != -1) {
                    DefaultListModel listModel = new DefaultListModel();
                    for (Enterprise en : currEnterprisesList) {
                        listModel.addElement(en.toString());
                    }

                    listEntrsOfSub.setModel(listModel);
                }
            }
        });
        panelRemoveEdges.add(removeEdge);

        panelEditFields.add(nameLabel);
        panelEditFields.add(nameEdt);
        panelEditFields.add(commentLabel);
        panelEditFields.add(commentEdt);
        panelEditFields.add(panelRemoveEdges);
        panelEditFields.add(panelEnterOfSub);
        panelEditFields.add(panelChangeDelete);
        frame.getContentPane().add(panelEditFields, BorderLayout.CENTER);
    }


    protected void initRightPanel(JFrame frame) {
        panelListEnter = new JPanel();
        panelListEnter.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 10));
        panelListEnter.setBackground(new Color(231, 226, 253));
        panelListEnter.setPreferredSize(new Dimension(200, 500));

        enterpriseList = Parser.getAllEnterprises();
//        listEnterprises = new JList(enterpriseList.toArray());
//        listEnterprises.setPreferredSize(new Dimension(200, 400));

        JScrollPane scrollPane = new JScrollPane();
        listEnterprises = new JList(enterpriseList.toArray());
        scrollPane.setViewportView(listEnterprises);
        scrollPane.setPreferredSize(new Dimension(200, 400));

        panelListEnter.add(new JLabel("Enterprises"));
        panelListEnter.add(scrollPane);

        JButton button = new JButton("Добавить к Subsystem");
        panelListEnter.add(button);
        button.addActionListener(e->{
            int index = listEnterprises.getSelectedIndex();
            Enterprise currEnter = enterpriseList.get(index);

            if (currEnterprisesList == null) currEnterprisesList = new ArrayList<>();

            for (Enterprise en : currEnterprisesList) {
                if (en.getId() == currEnter.getId()){
                    return;
                }
            }

            currEnterprisesList.add(currEnter);

            DefaultListModel listModel = new DefaultListModel();
            for (Enterprise en : currEnterprisesList) {
                listModel.addElement(en.toString());
            }

            if (listEntrsOfSub != null) {
                listEntrsOfSub.setModel(listModel);
            } else {
                listEntrsOfSub = new JList(listModel);
                listEntrsOfSub.setPreferredSize(new Dimension(400, 100));
                panelEnterOfSub.add(listEntrsOfSub);
            }

            panelEnterOfSub.revalidate();
            panelEnterOfSub.repaint();
        });


        frame.getContentPane().add(panelListEnter, BorderLayout.EAST);
    }

    protected void initBottomPanel (JFrame frame) {
        panelButtom = new JPanel();
        panelButtom.setLayout(new FlowLayout());
        panelButtom.setBackground(new Color(231, 226, 253));
        panelButtom.setPreferredSize(new Dimension(700, 50));

        messageLabel = new JLabel();
        panelButtom.add(messageLabel);
        frame.add(panelButtom, BorderLayout.SOUTH);


    }


    protected void clearAll() {
        removeEdge.setVisible(false);
        listSubsystem.clearSelection();
        nameEdt.setText("");
        commentEdt.setText("");
        initEnterprisesBySubsystem(null);
    }

    protected void initEnterprisesBySubsystem (Subsystem subsystem) {

        panelEnterOfSub.removeAll();
        if (currEnterprisesList != null) currEnterprisesList.clear();


        if (subsystem != null) {
            currEnterprisesList = Parser.getEnterprisesBySubsystem(subsystem);
            listEntrsOfSub = new JList(currEnterprisesList.toArray());
        } else {
            listEntrsOfSub = new JList(new String[1]);
        }

        listEntrsOfSub.setPreferredSize(new Dimension(400, 100));
        panelEnterOfSub.add(listEntrsOfSub);


        listEntrsOfSub.addListSelectionListener(e -> {
            removeEdge.setVisible(true);
            /**
             * @TODO delete subsystems?!!!
             */
        });

        panelEnterOfSub.revalidate();
        panelEnterOfSub.repaint();
    }
}
