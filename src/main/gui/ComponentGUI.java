package main.gui;

import main.Parser;
import main.dao.Component;
import main.dao.Enterprise;
import main.dao.Subsystem;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ComponentGUI {

    private JFrame frame;
    private JPanel panelEditFields;
    private JPanel panelListComp;
    private JPanel panelListSub;
    private JPanel panelSubOfComp;
    private JPanel panelButtom;
    private JPanel panelChangeDelete;
    private JLabel nameLabel;
    private JTextField nameEdt;
    private JLabel commentLabel;
    private JTextField commentEdt;
    private JLabel vendorLabel;
    private JTextField vendorEdt;
    private JButton buttonAdd;
    private JButton buttonChange;
    private JButton buttonDelete;
    private JButton buttonForAdd;
    private JList listSubsystems;
    private JList listComponents;
    private JList listSubsOfComp;
    private JLabel messageLabel;
    private JButton removeEdge;

    private List<Component> componentsList;
    private List<Subsystem> subsystemsList;
    private List<Subsystem> currSubsystemsList;

    private Boolean currIsNative = null;

    private abstract class ThreadComponent extends Thread {
        protected Component component;

        public void setComponent(Component component) {
            this.component = component;
        }
    }

    private class ThreadChange extends ThreadComponent {
        @Override
        public void run() {
            Parser.changeComponent(component,
                    currSubsystemsList.size() == 0 ? null : currSubsystemsList.get(0),
                    currIsNative == null ? true : currIsNative);
        }
    }

    private class ThreadDelete extends ThreadComponent {
        @Override
        public void run() {
            Parser.deleteComponent(component);
        }
    }

    public void initGUI(){
        frame = new JFrame("Components");
        frame.setSize(900, 600);
        frame.setResizable(false);
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLayout(new BorderLayout());

        initLeftPanel(frame);
        initRightPanel(frame);
        initCentrePanel(frame);
        initBottomPanel(frame);
    }

    protected void initLeftPanel(JFrame frame) {
        panelListComp = new JPanel();
        panelListComp.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 10));
        panelListComp.setBackground(new Color(231, 226, 253));
        panelListComp.setPreferredSize(new Dimension(200, 500));

        componentsList = Parser.getAllComponents();
//        listComponents = new JList(componentsList.toArray());
//        listComponents.setPreferredSize(new Dimension(200, 400));

        JScrollPane scrollPane = new JScrollPane();
        listComponents = new JList(componentsList.toArray());
        scrollPane.setViewportView(listComponents);
        scrollPane.setPreferredSize(new Dimension(200, 400));


        listComponents.addListSelectionListener(e -> {

        });

        panelListComp.add(new JLabel("Components"));
        panelListComp.add(scrollPane);
        JButton btnSelect = new JButton("Выбрать");
        btnSelect.addActionListener(e->{
            int index = listComponents.getSelectedIndex();
            if (index >= 0) {
                Component selectedComp = componentsList.get(index);
                nameEdt.setText(selectedComp.getCompName());
                commentEdt.setText(selectedComp.getComment());
                vendorEdt.setText(selectedComp.getVendor());
                buttonAdd.setVisible(false);
                buttonChange.setVisible(true);
                buttonDelete.setVisible(true);
                initSubsystemsByComponents(selectedComp);
            }
        });
        panelListComp.add(btnSelect);
        buttonForAdd = new JButton("Добавить новый");
        panelListComp.add(buttonForAdd);
        buttonForAdd.addActionListener(e -> {
            buttonAdd.setVisible(true);
            buttonChange.setVisible(false);
            buttonDelete.setVisible(false);
            clearAll();
        });

        frame.getContentPane().add(panelListComp, BorderLayout.WEST);
    }

    protected void initCentrePanel(JFrame frame) {
        panelEditFields = new JPanel();
        panelEditFields.setBackground(new Color(231, 226, 253));
        panelEditFields.setPreferredSize(new Dimension(500, 500));
        panelEditFields.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 10));

        nameLabel = new JLabel("Name");
        nameLabel.setPreferredSize(new Dimension(400, 20));

        nameEdt = new JTextField();
        nameEdt.setPreferredSize(new Dimension(400, 30));

        commentLabel = new JLabel("Comment");
        commentLabel.setPreferredSize(new Dimension(400, 20));

        commentEdt = new JTextField();
        commentEdt.setPreferredSize(new Dimension(400, 30));

        vendorLabel = new JLabel("Vendor");
        vendorLabel.setPreferredSize(new Dimension(400, 20));

        vendorEdt = new JTextField();
        vendorEdt.setPreferredSize(new Dimension(400, 30));

        buttonAdd = new JButton("Добавить");
        buttonAdd.setPreferredSize(new Dimension(300, 30));

        buttonChange = new JButton("Изменить");
        buttonChange.setPreferredSize(new Dimension(150, 30));
        buttonChange.setVisible(false);

        buttonDelete = new JButton("Удалить");
        buttonDelete.setPreferredSize(new Dimension(150, 30));
        buttonDelete.setVisible(false);

        panelSubOfComp = new JPanel();
        panelSubOfComp.setPreferredSize(new Dimension(400, 100));
        panelSubOfComp.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));

        panelChangeDelete = new JPanel();
        panelChangeDelete.setPreferredSize(new Dimension(400, 30));
        panelChangeDelete.setBackground(new Color(231, 226, 253));
        panelChangeDelete.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));

        panelChangeDelete.add(buttonAdd);
        panelChangeDelete.add(buttonChange);
        panelChangeDelete.add(buttonDelete);


        buttonAdd.addActionListener(e -> {
            String strName = nameEdt.getText();
            String strComment = commentEdt.getText();
            String strVendor = vendorEdt.getText();

            if (!strComment.isEmpty() && !strName.isEmpty()) {
                Component component = new Component(strName, strComment, strVendor);
                if (currSubsystemsList != null && currIsNative != null) {
                    Component comp = Parser.addComponent(component, currSubsystemsList.get(0), currIsNative);
                    if (comp != null) {
                        messageLabel.setText("Добавленно");
                        clearAll();
                        componentsList.add(comp);

                        DefaultListModel listModel = new DefaultListModel();
                        for (Component item : componentsList) {
                            listModel.addElement(item.toString());
                        }
                        listComponents.setModel(listModel);

                    } else {
                        messageLabel.setText("Ошибка при добавленно");
                    }
                } else {
                    messageLabel.setText("Нужно добавить хотя бы один Subsystem!");
                }

            }
        });


        buttonChange.addActionListener(e -> {
            int index = listComponents.getSelectedIndex();

            String strName = nameEdt.getText();
            String strComment = commentEdt.getText();
            String strVendor = commentEdt.getText();

            if (!strComment.isEmpty() && !strName.isEmpty()) {
                if (index >= 0) {
                    Component component = componentsList.get(index);
                    if (!strComment.isEmpty()) component.setComment(strComment);
                    if (!strName.isEmpty()) component.setCompName(strName);
                    if (!strVendor.isEmpty()) component.setVendor(strVendor);

                    ThreadComponent myThread = new ThreadChange();
                    myThread.setComponent(component);
                    myThread.start();

                    messageLabel.setText("Сохраннено");
                    clearAll();
                }
            }
        });

        buttonDelete.addActionListener(e -> {
            int index = listComponents.getSelectedIndex();

            if (index >= 0) {
                Component component = componentsList.get(index);

                int dialogResult = JOptionPane.showConfirmDialog (null, "Вы действительно хотите удалить данную Component запись?");
                if(dialogResult == JOptionPane.YES_OPTION){

                    ThreadComponent myThread = new ThreadDelete();
                    myThread.setComponent(component);
                    myThread.start();

                    messageLabel.setText("Удаленно");
                    componentsList.remove(index);
                    clearAll();
                    DefaultListModel listModel = new DefaultListModel();
                    for (Component item : componentsList) {
                        listModel.addElement(item.toString());
                    }
                    listComponents.setModel(listModel);
                }
            }

        });


//        labelSubs.setPreferredSize(new Dimension(400 , 30));
        JPanel panelRemoveEdges = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));
        panelRemoveEdges.setPreferredSize(new Dimension(400 , 30));
        panelRemoveEdges.setBackground(new Color(231, 226, 253));
        JLabel labelSubs = new JLabel("Subsystems-Components");
        panelRemoveEdges.add(labelSubs);

        removeEdge = new JButton("Убрать Subsystem");
        removeEdge.setVisible(false);
        removeEdge.addActionListener(e -> {

            if (listSubsOfComp != null) {
                int index = listSubsOfComp.getSelectedIndex();
                currSubsystemsList.remove(index);

                if (index != -1) {
                    DefaultListModel listModel = new DefaultListModel();
                    for (Subsystem en : currSubsystemsList) {
                        listModel.addElement(en.toString());
                    }

                    listSubsOfComp.setModel(listModel);
                }
            }
        });
        panelRemoveEdges.add(removeEdge);


        panelEditFields.add(nameLabel);
        panelEditFields.add(nameEdt);
        panelEditFields.add(commentLabel);
        panelEditFields.add(commentEdt);
        panelEditFields.add(vendorLabel);
        panelEditFields.add(vendorEdt);
        panelEditFields.add(panelRemoveEdges);
        panelEditFields.add(panelSubOfComp);
        panelEditFields.add(panelChangeDelete);
        frame.getContentPane().add(panelEditFields, BorderLayout.CENTER);
    }

    protected void initRightPanel(JFrame frame) {
        panelListSub = new JPanel();
        panelListSub.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 10));
        panelListSub.setBackground(new Color(231, 226, 253));
        panelListSub.setPreferredSize(new Dimension(200, 500));

        subsystemsList = Parser.getAllSubsystems();
//        listSubsystems = new JList(subsystemsList.toArray());
//        listSubsystems.setPreferredSize(new Dimension(200, 400));

        JScrollPane scrollPane = new JScrollPane();
        listSubsystems = new JList(subsystemsList.toArray());
        scrollPane.setViewportView(listSubsystems);
        scrollPane.setPreferredSize(new Dimension(200, 400));

        panelListSub.add(new JLabel("Subsystems"));
        panelListSub.add(scrollPane);

        JButton button = new JButton("Добавить к Component");
        panelListSub.add(button);
        button.addActionListener(e->{
            int index = listSubsystems.getSelectedIndex();
            Subsystem currSub = subsystemsList.get(index);

            String[] options = new String[] {"True", "False"};
            int res = JOptionPane.showOptionDialog(frame,
                    "Какое значение параметра isnative?",
                    "Is native",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,     //do not use a custom Icon
                    options,  //the titles of buttons
                    options[0]);

            if (res == JOptionPane.YES_OPTION) {
                currIsNative = true;
            } else {
                currIsNative = false;
            }

            currSubsystemsList = new ArrayList<>();

            for (Subsystem s : currSubsystemsList) {
                if (s.getId() == currSub.getId()){
                    return;
                }
            }

            currSubsystemsList.add(currSub);

            DefaultListModel listModel = new DefaultListModel();
            for (Subsystem s : currSubsystemsList) {
                listModel.addElement(s.toString());
            }

            if (listSubsOfComp != null) {
                listSubsOfComp.setModel(listModel);
            } else {
                listSubsOfComp = new JList(listModel);
                listSubsOfComp.setPreferredSize(new Dimension(400, 100));
                panelSubOfComp.add(listSubsOfComp);
            }

            panelSubOfComp.revalidate();
            panelSubOfComp.repaint();
        });


        frame.getContentPane().add(panelListSub, BorderLayout.EAST);
    }

    protected void initBottomPanel (JFrame frame) {
        panelButtom = new JPanel();
        panelButtom.setLayout(new FlowLayout());
        panelButtom.setBackground(new Color(231, 226, 253));
        panelButtom.setPreferredSize(new Dimension(700, 50));

        messageLabel = new JLabel();
        panelButtom.add(messageLabel);
        frame.add(panelButtom, BorderLayout.SOUTH);


    }

    protected void clearAll() {
        removeEdge.setVisible(false);
        currIsNative = null;
        listComponents.clearSelection();
        nameEdt.setText("");
        commentEdt.setText("");
        vendorEdt.setText("");
        initSubsystemsByComponents(null);
    }

    protected void initSubsystemsByComponents (Component component) {

        panelSubOfComp.removeAll();
        if (currSubsystemsList != null) currSubsystemsList.clear();


        if (component != null) {
            currSubsystemsList = Parser.getSubsystemByComponent(component);
            listSubsOfComp = new JList(currSubsystemsList.toArray());
        } else {
            listSubsOfComp = new JList(new String[1]);
        }

        listSubsOfComp.setPreferredSize(new Dimension(400, 100));
        panelSubOfComp.add(listSubsOfComp);


        listSubsOfComp.addListSelectionListener(e -> {
            removeEdge.setVisible(true);
        });

        panelSubOfComp.revalidate();
        panelSubOfComp.repaint();
    }
}
