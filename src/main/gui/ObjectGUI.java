package main.gui;

import main.Parser;
import main.dao.Enterprise;
import main.dao.MyObject;
import main.dao.ObjGroup;
import main.dao.Subsystem;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ObjectGUI {

    private JFrame frame;
    private JPanel panelEditFields;
    private JPanel panelListObject;
    private JPanel panelListObjGrp;
    private JPanel panelObjGrpOfObj;
    private JPanel panelButtom;
    private JPanel panelChangeDelete;
    private JLabel nameLabel;
    private JTextField nameEdt;
    private JLabel commentLabel;
    private JTextField commentEdt;
    private JButton buttonAdd;
    private JButton buttonChange;
    private JButton buttonDelete;
    private JButton buttonForAdd;
    private JList listObjGroups;
    private JList listObjects;
    private JList listObjGrpsOfObject;
    private JLabel messageLabel;
    private JButton removeEdge;

    private List<ObjGroup> objGroupList;
    private List<MyObject> myObjectList;
    private List<ObjGroup> currobjGroupsList;

    private abstract class ThreadObject extends Thread {
        protected MyObject myObject;

        public void setMyObject(MyObject myObject) {
            this.myObject = myObject;
        }
    }

    private class ThreadChange extends ThreadObject {
        @Override
        public void run() {
            Parser.changeObject(myObject,  currobjGroupsList.size() == 0 ? null : currobjGroupsList.get(0));
        }
    }

    private class ThreadDelete extends ThreadObject {
        @Override
        public void run() {
            Parser.deleteObject(myObject);
        }
    }

    public void initGUI(){
        frame = new JFrame("Objects");
        frame.setSize(900, 600);
        frame.setResizable(false);
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLayout(new BorderLayout());

        initLeftPanel(frame);
        initRightPanel(frame);
        initCentrePanel(frame);
        initBottomPanel(frame);
    }

    protected void initLeftPanel(JFrame frame) {
        panelListObject = new JPanel();
        panelListObject.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 10));
        panelListObject.setBackground(new Color(231, 226, 253));
        panelListObject.setPreferredSize(new Dimension(200, 500));

        myObjectList = Parser.getAllObjects();
//        listObjects = new JList(myObjectList.toArray());
//        listObjects.setPreferredSize(new Dimension(200, 400));

        JScrollPane scrollPane = new JScrollPane();
        listObjects = new JList(myObjectList.toArray());
        scrollPane.setViewportView(listObjects);
        scrollPane.setPreferredSize(new Dimension(200, 400));


        listObjects.addListSelectionListener(e -> {

        });

        panelListObject.add(new JLabel("Objects"));
        JButton btnSelect = new JButton("Выбрать");
        btnSelect.addActionListener(e->{
            int index = listObjects.getSelectedIndex();
            if (index >= 0) {
                MyObject selectedMyObject = myObjectList.get(index);
                nameEdt.setText(selectedMyObject.getObjname());
                commentEdt.setText(selectedMyObject.getBelongsto());
                buttonAdd.setVisible(false);
                buttonChange.setVisible(true);
                buttonDelete.setVisible(true);
                initObjectByObjectGroup(selectedMyObject);
            }
        });
        panelListObject.add(scrollPane);
        panelListObject.add(btnSelect);
        buttonForAdd = new JButton("Добавить новый");
        panelListObject.add(buttonForAdd);
        buttonForAdd.addActionListener(e -> {
            buttonAdd.setVisible(true);
            buttonChange.setVisible(false);
            buttonDelete.setVisible(false);
            clearAll();
        });

        frame.getContentPane().add(panelListObject, BorderLayout.WEST);
    }


    protected void initCentrePanel(JFrame frame) {
        panelEditFields = new JPanel();
        panelEditFields.setBackground(new Color(231, 226, 253));
        panelEditFields.setPreferredSize(new Dimension(500, 500));
        panelEditFields.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 10));

        nameLabel = new JLabel("Name");
        nameLabel.setPreferredSize(new Dimension(400, 20));

        nameEdt = new JTextField();
        nameEdt.setPreferredSize(new Dimension(400, 30));

        commentLabel = new JLabel("Belongsto");
        commentLabel.setPreferredSize(new Dimension(400, 20));

        commentEdt = new JTextField();
        commentEdt.setPreferredSize(new Dimension(400, 30));

        buttonAdd = new JButton("Добавить");
        buttonAdd.setPreferredSize(new Dimension(300, 30));

        buttonChange = new JButton("Изменить");
        buttonChange.setPreferredSize(new Dimension(150, 30));
        buttonChange.setVisible(false);

        buttonDelete = new JButton("Удалить");
        buttonDelete.setPreferredSize(new Dimension(150, 30));
        buttonDelete.setVisible(false);

        panelObjGrpOfObj = new JPanel();
        panelObjGrpOfObj.setPreferredSize(new Dimension(400, 100));
        panelObjGrpOfObj.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));

        panelChangeDelete = new JPanel();
        panelChangeDelete.setPreferredSize(new Dimension(400, 30));
        panelChangeDelete.setBackground(new Color(231, 226, 253));
        panelChangeDelete.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));

        panelChangeDelete.add(buttonAdd);
        panelChangeDelete.add(buttonChange);
        panelChangeDelete.add(buttonDelete);


        buttonAdd.addActionListener(e -> {
            String strName = nameEdt.getText();
            String strComment = commentEdt.getText();

            if (!strComment.isEmpty() && !strName.isEmpty()) {
                MyObject myObject = new MyObject(strName, strComment);
                if (currobjGroupsList != null) {
                    MyObject obj = Parser.addObject(myObject, currobjGroupsList.get(0));
                    if (obj != null) {
                        messageLabel.setText("Добавленно");
                        clearAll();
                        myObjectList.add(obj);

                        DefaultListModel listModel = new DefaultListModel();
                        for (MyObject item : myObjectList) {
                            listModel.addElement(item.toString());
                        }
                        listObjects.setModel(listModel);

                    } else {
                        messageLabel.setText("Ошибка при добавленно");
                    }
                } else {
                    messageLabel.setText("Нужно добавить хотя бы один Enterprise!");
                }

            }
        });


        buttonChange.addActionListener(e -> {
            int index = listObjects.getSelectedIndex();

            String strName = nameEdt.getText();
            String strComment = commentEdt.getText();

            if (!strComment.isEmpty() && !strName.isEmpty()) {
                if (index >= 0) {
                    MyObject myObject = myObjectList.get(index);
                    if (!strComment.isEmpty()) myObject.setBelongsto(strComment);
                    if (!strName.isEmpty()) myObject.setObjname(strName);

                    ThreadObject myThread = new ThreadChange();
                    myThread.setMyObject(myObject);
                    myThread.start();

                    messageLabel.setText("Сохраннено");
                    clearAll();
                }
            }
        });

        buttonDelete.addActionListener(e -> {
            int index = listObjects.getSelectedIndex();

            if (index >= 0) {
                MyObject myObject = myObjectList.get(index);

                int dialogResult = JOptionPane.showConfirmDialog (null, "Вы действительно хотите удалить данную Object запись?");
                if(dialogResult == JOptionPane.YES_OPTION){

                    ThreadObject myThread = new ThreadDelete();
                    myThread.setMyObject(myObject);
                    myThread.start();

                    messageLabel.setText("Удаленно");
                    myObjectList.remove(index);
                    clearAll();
                    DefaultListModel listModel = new DefaultListModel();
                    for (MyObject item : myObjectList) {
                        listModel.addElement(item.toString());
                    }
                    listObjects.setModel(listModel);
                }
            }

        });


//        labelSubs.setPreferredSize(new Dimension(400 , 30));

        JPanel panelRemoveEdges = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));
        panelRemoveEdges.setPreferredSize(new Dimension(400 , 30));
        panelRemoveEdges.setBackground(new Color(231, 226, 253));
        JLabel labelSubs = new JLabel("ObjectGroups-Objects");
        panelRemoveEdges.add(labelSubs);

        removeEdge = new JButton("Убрать ObjectGroup");
        removeEdge.setVisible(false);
        removeEdge.addActionListener(e -> {

            if (listObjGrpsOfObject != null) {
                int index = listObjGrpsOfObject.getSelectedIndex();
                currobjGroupsList.remove(index);

                if (index != -1) {
                    DefaultListModel listModel = new DefaultListModel();
                    for (ObjGroup en : currobjGroupsList) {
                        listModel.addElement(en.toString());
                    }

                    listObjGrpsOfObject.setModel(listModel);
                }
            }
        });
        panelRemoveEdges.add(removeEdge);

        panelEditFields.add(nameLabel);
        panelEditFields.add(nameEdt);
        panelEditFields.add(commentLabel);
        panelEditFields.add(commentEdt);
        panelEditFields.add(panelRemoveEdges);
        panelEditFields.add(panelObjGrpOfObj);
        panelEditFields.add(panelChangeDelete);
        frame.getContentPane().add(panelEditFields, BorderLayout.CENTER);
    }

    protected void initRightPanel(JFrame frame) {
        panelListObjGrp = new JPanel();
        panelListObjGrp.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 10));
        panelListObjGrp.setBackground(new Color(231, 226, 253));
        panelListObjGrp.setPreferredSize(new Dimension(200, 500));

        objGroupList = Parser.getAllObjGroups();
//        listObjGroups = new JList(objGroupList.toArray());
//        listObjGroups.setPreferredSize(new Dimension(200, 400));

        JScrollPane scrollPane = new JScrollPane();
        listObjGroups = new JList(objGroupList.toArray());
        scrollPane.setViewportView(listObjGroups);
        scrollPane.setPreferredSize(new Dimension(200, 400));

        panelListObjGrp.add(new JLabel("ObjectGroups"));
        panelListObjGrp.add(scrollPane);

        JButton button = new JButton("Добавить к Object");
        panelListObjGrp.add(button);
        button.addActionListener(e->{
            int index = listObjGroups.getSelectedIndex();
            ObjGroup currObjGroup = objGroupList.get(index);

            currobjGroupsList = new ArrayList<>();

//            for (ObjGroup og : currobjGroupsList) {
//                if (og.getId() == currObjGroup.getId()){
//                    return;
//                }
//            }

            currobjGroupsList.add(currObjGroup);

            DefaultListModel listModel = new DefaultListModel();
            for (ObjGroup og : currobjGroupsList) {
                listModel.addElement(og.toString());
            }

            if (listObjGrpsOfObject != null) {
                listObjGrpsOfObject.setModel(listModel);
            } else {
                listObjGrpsOfObject = new JList(listModel);
                listObjGrpsOfObject.setPreferredSize(new Dimension(400, 100));
                panelObjGrpOfObj.add(listObjGrpsOfObject);
            }

            panelObjGrpOfObj.revalidate();
            panelObjGrpOfObj.repaint();
        });


        frame.getContentPane().add(panelListObjGrp, BorderLayout.EAST);
    }

    protected void initBottomPanel (JFrame frame) {
        panelButtom = new JPanel();
        panelButtom.setLayout(new FlowLayout());
        panelButtom.setBackground(new Color(231, 226, 253));
        panelButtom.setPreferredSize(new Dimension(700, 50));

        messageLabel = new JLabel();
        panelButtom.add(messageLabel);
        frame.add(panelButtom, BorderLayout.SOUTH);


    }

    protected void clearAll() {
        removeEdge.setVisible(false);
        listObjects.clearSelection();
        nameEdt.setText("");
        commentEdt.setText("");
        initObjectByObjectGroup(null);
    }

    protected void initObjectByObjectGroup (MyObject myObject) {

        panelObjGrpOfObj.removeAll();
        if (currobjGroupsList != null) currobjGroupsList.clear();


        if (myObject != null) {
            currobjGroupsList = Parser.getObjGroupByObject(myObject);
            listObjGrpsOfObject = new JList(currobjGroupsList.toArray());
        } else {
            listObjGrpsOfObject = new JList(new String[1]);
        }

        listObjGrpsOfObject.setPreferredSize(new Dimension(400, 100));
        panelObjGrpOfObj.add(listObjGrpsOfObject);


        listObjGrpsOfObject.addListSelectionListener(e -> {
            removeEdge.setVisible(true);
            /**
             * @TODO delete subsystems?!!!
             */
        });


        panelObjGrpOfObj.revalidate();
        panelObjGrpOfObj.repaint();
    }
}
