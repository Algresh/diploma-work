package main.gui;

import main.Parser;
import main.dao.Enterprise;
import main.dao.Subsystem;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class EnterpriseGUI {

    private JFrame frame;
    private JPanel panelEditFields;
    private JPanel panelListEnter;
    private JPanel panelListSub;
    private JPanel panelSubOfEnter;
    private JPanel panelButtom;
    private JPanel panelChangeDelete;
    private JLabel nameLabel;
    private JTextField nameEdt;
    private JLabel commentLabel;
    private JTextField commentEdt;
    private JButton buttonAdd;
    private JButton buttonChange;
    private JButton buttonDelete;
    private JButton buttonForAdd;
    private JList listEnterprises;
    private JList listSubsystem;
    private JList listSubsOfEntr;
    private JLabel messageLabel;
    private JButton removeEdge;

    private List<Enterprise> enterpriseList;
    private List<Subsystem> subsystemList;
    private List<Subsystem> currSubsystemList;


    public void initGUI(){
        frame = new JFrame("Enterprise");
        frame.setSize(900, 600);
        frame.setResizable(false);
//        frame.setPreferredSize(new Dimension(900, 600));
//        frame.setMaximumSize(new Dimension(900, 600));
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLayout(new BorderLayout());

        initLeftPanel(frame);
        initRightPanel(frame);
        initCentrePanel(frame);
        initBottomPanel(frame);
    }


    protected void initLeftPanel(JFrame frame) {
        panelListEnter = new JPanel();
        panelListEnter.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 10));
        panelListEnter.setBackground(new Color(231, 226, 253));
        panelListEnter.setPreferredSize(new Dimension(200, 500));

        enterpriseList = Parser.getAllEnterprises();
//        listEnterprises = new JList(enterpriseList.toArray());
//        listEnterprises.setPreferredSize(new Dimension(200, 400));

        JScrollPane scrollPane = new JScrollPane();
        listEnterprises = new JList(enterpriseList.toArray());
        scrollPane.setViewportView(listEnterprises);
        scrollPane.setPreferredSize(new Dimension(200, 400));

        listEnterprises.addListSelectionListener(e -> {

        });

        panelListEnter.add(new JLabel("Enterprises"));
        panelListEnter.add(scrollPane);
        JButton btnSelect = new JButton("Выбрать");
        btnSelect.addActionListener(e -> {
            int index = listEnterprises.getSelectedIndex();
            if (index >= 0) {
                Enterprise selectedEnter = enterpriseList.get(index);
                nameEdt.setText(selectedEnter.getEnterpriseName());
                commentEdt.setText(selectedEnter.getComment());
                buttonAdd.setVisible(false);
                buttonChange.setVisible(true);
                buttonDelete.setVisible(true);
                initSubsystemsByEnterprise(selectedEnter);
            }
        });
        panelListEnter.add(btnSelect);
        buttonForAdd = new JButton("Добавить новый");
        panelListEnter.add(buttonForAdd);
        buttonForAdd.addActionListener(e -> {
            buttonAdd.setVisible(true);
            buttonChange.setVisible(false);
            buttonDelete.setVisible(false);
            clearAll();
        });

        frame.getContentPane().add(panelListEnter, BorderLayout.WEST);
    }

    protected void initCentrePanel(JFrame frame) {
        panelEditFields = new JPanel();
        panelEditFields.setBackground(new Color(231, 226, 253));
        panelEditFields.setPreferredSize(new Dimension(500, 500));
        panelEditFields.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 10));

        nameLabel = new JLabel("Name");
        nameLabel.setPreferredSize(new Dimension(400, 20));

        nameEdt = new JTextField();
        nameEdt.setPreferredSize(new Dimension(400, 30));

        commentLabel = new JLabel("Comment");
        commentLabel.setPreferredSize(new Dimension(400, 20));

        commentEdt = new JTextField();
        commentEdt.setPreferredSize(new Dimension(400, 30));

        buttonAdd = new JButton("Добавить");
        buttonAdd.setPreferredSize(new Dimension(300, 30));

        buttonChange = new JButton("Изменить");
        buttonChange.setPreferredSize(new Dimension(150, 30));
        buttonChange.setVisible(false);

        buttonDelete = new JButton("Удалить");
        buttonDelete.setPreferredSize(new Dimension(150, 30));
        buttonDelete.setVisible(false);

        panelSubOfEnter = new JPanel();
        panelSubOfEnter.setPreferredSize(new Dimension(400, 100));
        panelSubOfEnter.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));

        panelChangeDelete = new JPanel();
        panelChangeDelete.setPreferredSize(new Dimension(400, 30));
        panelChangeDelete.setBackground(new Color(231, 226, 253));
        panelChangeDelete.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));

        panelChangeDelete.add(buttonAdd);
        panelChangeDelete.add(buttonChange);
        panelChangeDelete.add(buttonDelete);


        buttonAdd.addActionListener(e -> {
            String strName = nameEdt.getText();
            String strComment = commentEdt.getText();

            if (!strComment.isEmpty() && !strName.isEmpty()) {
                Enterprise enterprise = new Enterprise(strName, strComment);
                if (currSubsystemList != null) {
                    Parser.addEnterprise(enterprise, currSubsystemList);
                } else {
                    Parser.addEnterprise(enterprise);
                }
                messageLabel.setText("Добавленно");
                clearAll();
            }
        });


        buttonChange.addActionListener(e -> {
            int index = listEnterprises.getSelectedIndex();

            String strName = nameEdt.getText();
            String strComment = commentEdt.getText();

            if (!strComment.isEmpty() && !strName.isEmpty()) {
                if (index >= 0) {
                    Enterprise enterprise = enterpriseList.get(index);
                    if (!strComment.isEmpty()) enterprise.setComment(strComment);
                    if (!strName.isEmpty()) enterprise.setEnterpriseName(strName);

//                    System.out.print(Parser.changeEnterprise(enterprise, currSubsystemList));
                    ThreadChange threadChange = new ThreadChange();
                    threadChange.setEnterprise(enterprise);
                    threadChange.start();

                    messageLabel.setText("Сохраннено");
                    clearAll();
                }
            }
        });

        buttonDelete.addActionListener(e -> {
            int index = listEnterprises.getSelectedIndex();

            if (index >= 0) {
                Enterprise enterprise = enterpriseList.get(index);

                int dialogResult = JOptionPane.showConfirmDialog (null, "Вы действительно хотите удалить данную Enterprise запись?");
                if(dialogResult == JOptionPane.YES_OPTION){
//                    System.out.print(Parser.deleteEnterprise(enterprise));

                    ThreadDelete threadDelete = new ThreadDelete();
                    threadDelete.setEnterprise(enterprise);
                    threadDelete.start();

                    messageLabel.setText("Удаленно");
                    enterpriseList.remove(index);
                    clearAll();
                    DefaultListModel listModel = new DefaultListModel();
                    for (Enterprise item : enterpriseList) {
                        listModel.addElement(item.toString());
                    }
                    listEnterprises.setModel(listModel);
                }
            }

        });

        JPanel panelRemoveEdges = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));
        panelRemoveEdges.setPreferredSize(new Dimension(400 , 30));
        panelRemoveEdges.setBackground(new Color(231, 226, 253));
        JLabel labelSubs = new JLabel("Enterprises-Subsystems");
        panelRemoveEdges.add(labelSubs);

        removeEdge = new JButton("Убрать Subsystem");
        removeEdge.setVisible(false);
        removeEdge.addActionListener(e -> {

            if (listSubsOfEntr != null) {
                int index = listSubsOfEntr.getSelectedIndex();
                currSubsystemList.remove(index);

                if (index != -1) {
                    DefaultListModel listModel = new DefaultListModel();
                    for (Subsystem s : currSubsystemList) {
                        listModel.addElement(s.toString());
                    }

                    listSubsOfEntr.setModel(listModel);
                }
            }
        });
        panelRemoveEdges.add(removeEdge);

        panelEditFields.add(nameLabel);
        panelEditFields.add(nameEdt);
        panelEditFields.add(commentLabel);
        panelEditFields.add(commentEdt);
        panelEditFields.add(panelRemoveEdges);
        panelEditFields.add(panelSubOfEnter);
        panelEditFields.add(panelChangeDelete);
        frame.getContentPane().add(panelEditFields, BorderLayout.CENTER);
    }

    protected void initRightPanel(JFrame frame) {
        panelListSub = new JPanel();
        panelListSub.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 10));
        panelListSub.setBackground(new Color(231, 226, 253));
        panelListSub.setPreferredSize(new Dimension(200, 500));

        subsystemList = Parser.getAllSubsystems();
//        listSubsystem = new JList(subsystemList.toArray());
//        listSubsystem.setPreferredSize(new Dimension(200, 400));


        JScrollPane scrollPane = new JScrollPane();
        listSubsystem = new JList(subsystemList.toArray());
        scrollPane.setViewportView(listSubsystem);
        scrollPane.setPreferredSize(new Dimension(200, 400));

        panelListSub.add(new JLabel("Subsystems"));
        panelListSub.add(scrollPane);

        JButton button = new JButton("Добавить к Enterprise");
        panelListSub.add(button);
        button.addActionListener(e->{
            int index = listSubsystem.getSelectedIndex();
            Subsystem currSub = subsystemList.get(index);

            if (currSubsystemList == null) currSubsystemList = new ArrayList<>();

            for (Subsystem s : currSubsystemList) {
                if (s.getId() == currSub.getId()){
                    return;
                }
            }

            currSubsystemList.add(currSub);

            DefaultListModel listModel = new DefaultListModel();
            for (Subsystem s : currSubsystemList) {
                listModel.addElement(s.toString());
            }

            if (listSubsOfEntr != null) {
                listSubsOfEntr.setModel(listModel);
            } else {
                listSubsOfEntr = new JList(listModel);
                listSubsOfEntr.setPreferredSize(new Dimension(400, 100));
                panelSubOfEnter.add(listSubsOfEntr);
            }

            panelSubOfEnter.revalidate();
            panelSubOfEnter.repaint();
        });


        frame.getContentPane().add(panelListSub, BorderLayout.EAST);
    }

    protected void initBottomPanel (JFrame frame) {
        panelButtom = new JPanel();
        panelButtom.setLayout(new FlowLayout());
        panelButtom.setBackground(new Color(231, 226, 253));
        panelButtom.setPreferredSize(new Dimension(700, 50));

        messageLabel = new JLabel();
        panelButtom.add(messageLabel);
        frame.add(panelButtom, BorderLayout.SOUTH);


    }

    protected void initSubsystemsByEnterprise (Enterprise enterprise) {

        panelSubOfEnter.removeAll();
        if (currSubsystemList != null) currSubsystemList.clear();


        if (enterprise != null) {
            currSubsystemList = Parser.getSubsystemsByEnterprise(enterprise);
            listSubsOfEntr = new JList(currSubsystemList.toArray());
        } else {
            listSubsOfEntr = new JList(new String[1]);
        }

        listSubsOfEntr.setPreferredSize(new Dimension(400, 100));
        panelSubOfEnter.add(listSubsOfEntr);


        listSubsOfEntr.addListSelectionListener(e -> {
            removeEdge.setVisible(true);
        });

        panelSubOfEnter.revalidate();
        panelSubOfEnter.repaint();
    }


    protected void clearAll() {
        listEnterprises.clearSelection();
        nameEdt.setText("");
        commentEdt.setText("");
        removeEdge.setVisible(false);
        initSubsystemsByEnterprise(null);
    }


    private abstract class ThreadEnterprise extends Thread {
        protected Enterprise enterprise;
        public void setEnterprise(Enterprise enterprise) {
            this.enterprise = enterprise;
        }
    }

    private class ThreadChange extends ThreadEnterprise {
        @Override
        public void run() {
            Parser.changeEnterprise(enterprise, currSubsystemList);
        }
    }

    private class ThreadDelete extends ThreadEnterprise {
        @Override
        public void run() {
            Parser.deleteEnterprise(enterprise);
        }
    }

}
