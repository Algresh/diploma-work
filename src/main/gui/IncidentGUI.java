package main.gui;

import main.Parser;
import main.dao.*;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class IncidentGUI {

    private JFrame frame;
    private JPanel panelEditFields;
    private JPanel panelListIncident;
    private JPanel panelListObject;
    private JPanel panelObjectOfIncident;
    private JPanel panelIncidentOfIncident;
    private JPanel panelButtom;
    private JPanel panelChangeDelete;

    private JLabel codeLabel;
    private JTextField codeEdt;

    private JLabel messageLabel;
    private JTextField messageEdt;

    private JLabel severityLabel;
    private JTextField severityEdt;

    private JLabel typeLabel;
    private JTextField typeEdt;

    private JLabel eventLabel;
    private JTextField eventEdt;

    private JLabel actionLabel;
    private JTextField actionEdt;

    private JLabel osiLabel;
    private JTextField osiEdt;

    private JLabel commentLabel;
    private JTextField commentEdt;

    private JButton buttonAdd;
    private JButton buttonChange;
    private JButton buttonDelete;
    private JButton buttonForAdd;
    private JList listObject;
    private JList listIncident;
    private JList listObjOfIncident;
    private JList listIncOfInc;
    private JLabel messageLabelBottom;
    private JButton removeEdge;
    private JButton removeEdgeIncident;

    private List<MyObject> objectsList;
    private List<Incident> incidentList;
    private List<MyObject> currObjectsList;
    private List<Incident> currIncidentsList;

    private List<IncidentIncident> currIncidentIncidentList;
    private List<ObjectIncident> currObjectIncidentList;

    private abstract class ThreadIncident extends Thread {
        protected Incident incident;

        public void setIncident(Incident incident) {
            this.incident = incident;
        }
    }

    private class ThreadChange extends ThreadIncident {
        @Override
        public void run() {
            Parser.changeIncident(incident, currObjectIncidentList,
                    currIncidentIncidentList, currObjectsList, currIncidentsList);
        }
    }

    private class ThreadDelete extends ThreadIncident {
        @Override
        public void run() {
            Parser.deleteIncident(incident);
        }
    }

    public void initGUI(){
        frame = new JFrame("Incidents");
        frame.setSize(900, 900);
        frame.setResizable(false);
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLayout(new BorderLayout());

        initLeftPanel(frame);
        initRightPanel(frame);
        initCentrePanel(frame);
        initBottomPanel(frame);
    }


    protected void initLeftPanel(JFrame frame) {
        panelListIncident = new JPanel();
        panelListIncident.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 10));
        panelListIncident.setBackground(new Color(231, 226, 253));
        panelListIncident.setPreferredSize(new Dimension(200, 700));

        incidentList = Parser.getAllIncidents();
        JScrollPane scrollPane = new JScrollPane();
        listIncident = new JList(incidentList.toArray());
        scrollPane.setViewportView(listIncident);
        scrollPane.setPreferredSize(new Dimension(200, 600));
        listIncident.addListSelectionListener(e -> {

        });

        panelListIncident.add(new JLabel("Incidents"));
        JButton buttonAddLinkIncInc = new JButton("Добавить к Incident");
        buttonAddLinkIncInc.addActionListener(e->{
            int index = listIncident.getSelectedIndex();
            Incident currInc = incidentList.get(index);

            if (currIncidentsList == null) currIncidentsList = new ArrayList<>();

            for (Incident i : currIncidentsList) {
                if (i.getId() == currInc.getId()){
                    return;
                }
            }

            JTextField commentEdt = new JTextField();
            JCheckBox isSuedByCb = new JCheckBox();
            JCheckBox leadtoCB = new JCheckBox();
            final JComponent[] inputs = new JComponent[] {
                    new JLabel("Comment"),
                    commentEdt,
                    new JLabel("isSuedBy"),
                    isSuedByCb,
                    new JLabel("leadto"),
                    leadtoCB
            };

            int res = JOptionPane.showConfirmDialog(null, inputs, "Параметры edge", JOptionPane.PLAIN_MESSAGE);

            String commentStr = commentEdt.getText();
            boolean isSuedBy = isSuedByCb.isSelected();
            boolean isLeadto = leadtoCB.isSelected();
            if (res == JOptionPane.OK_OPTION && !commentStr.isEmpty()) {

                IncidentIncident incInc = new IncidentIncident(currInc.getId(), isSuedBy, isLeadto, commentStr);
                if (currIncidentIncidentList == null) currIncidentIncidentList = new ArrayList<>();
                currIncidentIncidentList.add(incInc);

                currIncidentsList.add(currInc);

                DefaultListModel listModel = new DefaultListModel();
                for (Incident i : currIncidentsList) {
                    listModel.addElement(i.toString());
                }

                if (listIncOfInc != null) {
                    listIncOfInc.setModel(listModel);
                } else {
                    listIncOfInc = new JList(listModel);
                    listIncOfInc.setPreferredSize(new Dimension(400, 100));
                    panelIncidentOfIncident.add(listIncOfInc);
                }

                panelIncidentOfIncident.revalidate();
                panelIncidentOfIncident.repaint();
            }

        });
        panelListIncident.add(buttonAddLinkIncInc);
        panelListIncident.add(scrollPane);
        buttonForAdd = new JButton("Добавить новый");

        JButton btnSelect = new JButton("Выбрать");
        btnSelect.addActionListener(e -> {
            int index = listIncident.getSelectedIndex();
            if (index >= 0) {
                Incident selectedIncident = incidentList.get(index);

                codeEdt.setText(selectedIncident.getCode());
                commentEdt.setText(selectedIncident.getComment());
                messageEdt.setText(selectedIncident.getMessage());
                severityEdt.setText(selectedIncident.getSeverity());
                typeEdt.setText(selectedIncident.getType());
                eventEdt.setText(selectedIncident.getEvent());
                actionEdt.setText(selectedIncident.getAction());
                osiEdt.setText(String.valueOf(selectedIncident.getOsi()));


                buttonAdd.setVisible(false);
                buttonChange.setVisible(true);
                buttonDelete.setVisible(true);
                initObjectsByIncident(selectedIncident);
                initIncidentsByIncident(selectedIncident);
            }
        });
        panelListIncident.add(btnSelect);
        panelListIncident.add(buttonForAdd);
        buttonForAdd.addActionListener(e -> {
            buttonAdd.setVisible(true);
            buttonChange.setVisible(false);
            buttonDelete.setVisible(false);
            clearAll();
        });

        frame.getContentPane().add(panelListIncident, BorderLayout.WEST);
    }

    protected void initCentrePanel(JFrame frame) {
        panelEditFields = new JPanel();
        panelEditFields.setBackground(new Color(231, 226, 253));
        panelEditFields.setPreferredSize(new Dimension(500, 500));
        panelEditFields.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        codeLabel = new JLabel("Code");
        codeLabel.setPreferredSize(new Dimension(400, 20));

        codeEdt = new JTextField();
        codeEdt.setPreferredSize(new Dimension(400, 30));

        messageLabel = new JLabel("Message");
        messageLabel.setPreferredSize(new Dimension(400, 20));

        messageEdt = new JTextField();
        messageEdt.setPreferredSize(new Dimension(400, 30));

        severityLabel = new JLabel("Severity");
        severityLabel.setPreferredSize(new Dimension(400, 20));

        severityEdt = new JTextField();
        severityEdt.setPreferredSize(new Dimension(400, 30));

        typeLabel = new JLabel("Type");
        typeLabel.setPreferredSize(new Dimension(400, 20));

        typeEdt = new JTextField();
        typeEdt.setPreferredSize(new Dimension(400, 30));

        eventLabel = new JLabel("Event");
        eventLabel.setPreferredSize(new Dimension(400, 20));

        eventEdt = new JTextField();
        eventEdt.setPreferredSize(new Dimension(400, 30));

        actionLabel = new JLabel("Action");
        actionLabel.setPreferredSize(new Dimension(400, 20));

        actionEdt = new JTextField();
        actionEdt.setPreferredSize(new Dimension(400, 30));

        osiLabel = new JLabel("OSI");
        osiLabel.setPreferredSize(new Dimension(400, 20));

        osiEdt = new JTextField();
        osiEdt.setPreferredSize(new Dimension(400, 30));

        commentLabel = new JLabel("Comment");
        commentLabel.setPreferredSize(new Dimension(400, 20));

        commentEdt = new JTextField();
        commentEdt.setPreferredSize(new Dimension(400, 30));

        buttonAdd = new JButton("Добавить");
        buttonAdd.setPreferredSize(new Dimension(300, 30));

        buttonChange = new JButton("Изменить");
        buttonChange.setPreferredSize(new Dimension(150, 30));
        buttonChange.setVisible(false);

        buttonDelete = new JButton("Удалить");
        buttonDelete.setPreferredSize(new Dimension(150, 30));
        buttonDelete.setVisible(false);

        panelObjectOfIncident = new JPanel();
        panelObjectOfIncident.setPreferredSize(new Dimension(400, 100));
        panelObjectOfIncident.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));

        panelIncidentOfIncident= new JPanel();
        panelIncidentOfIncident.setPreferredSize(new Dimension(400, 100));
        panelIncidentOfIncident.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));

        panelChangeDelete = new JPanel();
        panelChangeDelete.setPreferredSize(new Dimension(400, 30));
        panelChangeDelete.setBackground(new Color(231, 226, 253));
        panelChangeDelete.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));

        panelChangeDelete.add(buttonAdd);
        panelChangeDelete.add(buttonChange);
        panelChangeDelete.add(buttonDelete);


        buttonAdd.addActionListener(e -> {
            String strCode = codeEdt.getText();
            String strMsg = messageEdt.getText();
            String strSeverity = severityEdt.getText();
            String strType = typeEdt.getText();
            String strEvent = eventEdt.getText();
            String strAction = actionEdt.getText();
            String strOSI = osiEdt.getText();
            String strComment = commentEdt.getText();
//
            if (!strComment.isEmpty() && !strCode.isEmpty() && !strMsg.isEmpty() && !strSeverity.isEmpty()
                    && !strType.isEmpty() && !strEvent.isEmpty() && !strAction.isEmpty() && !strOSI.isEmpty()) {
                Incident incident = new Incident(strCode, strMsg, strSeverity, strType, strEvent, strAction,
                        Integer.parseInt(strOSI), strComment);
                if ((currIncidentIncidentList != null && !currIncidentIncidentList.isEmpty())
                        || (currObjectIncidentList != null && !currObjectIncidentList.isEmpty())) {

                    Incident inc = Parser.addIncident(incident, currObjectIncidentList, currIncidentIncidentList);
                    if (inc != null) {
                        messageLabel.setText("Добавленно");
                        clearAll();
                        incidentList.add(inc);

                        DefaultListModel listModel = new DefaultListModel();
                        for (Incident item : incidentList) {
                            listModel.addElement(item.toString());
                        }
                        listIncident.setModel(listModel);

                    } else {
                        messageLabelBottom.setText("Ошибка при добавленно");
                    }
                } else {
                    messageLabelBottom.setText("Нужно добавить хотя бы один Enterprise!");
                }
//
            }
        });


        buttonChange.addActionListener(e -> {
            int index = listIncident.getSelectedIndex();

            String strCode = codeEdt.getText();
            String strMsg = messageEdt.getText();
            String strSeverity = severityEdt.getText();
            String strType = typeEdt.getText();
            String strEvent = eventEdt.getText();
            String strAction = actionEdt.getText();
            String strOSI = osiEdt.getText();
            String strComment = commentEdt.getText();

            if (!strComment.isEmpty() && !strCode.isEmpty() && !strMsg.isEmpty() && !strSeverity.isEmpty()
                    && !strType.isEmpty() && !strEvent.isEmpty() && !strAction.isEmpty() && !strOSI.isEmpty()) {
                if (index >= 0) {
                    Incident incident = incidentList.get(index);

                    if (!strComment.isEmpty()) incident.setComment(strComment);
                    if (!strCode.isEmpty()) incident.setCode(strCode);
                    if (!strMsg.isEmpty()) incident.setMessage(strMsg);
                    if (!strSeverity.isEmpty()) incident.setSeverity(strSeverity);
                    if (!strType.isEmpty()) incident.setType(strType);
                    if (!strEvent.isEmpty()) incident.setEvent(strEvent);
                    if (!strAction.isEmpty()) incident.setAction(strAction);
                    if (!strOSI.isEmpty()) incident.setOsi(Integer.parseInt(strOSI));

                    ThreadIncident myThread = new ThreadChange();
                    myThread.setIncident(incident);
                    myThread.start();


                    messageLabelBottom.setText("Сохраннено");
                    clearAll();
                }
            }
        });

        buttonDelete.addActionListener(e -> {
            int index = listIncident.getSelectedIndex();

            if (index >= 0) {
                Incident incident = incidentList.get(index);

                int dialogResult = JOptionPane.showConfirmDialog (null, "Вы действительно хотите удалить данную Incident запись?");
                if(dialogResult == JOptionPane.YES_OPTION){

                    ThreadIncident myThread = new ThreadDelete();
                    myThread.setIncident(incident);
                    myThread.start();

                    messageLabelBottom.setText("Удаленно");
                    incidentList.remove(index);
                    clearAll();
                    DefaultListModel listModel = new DefaultListModel();
                    for (Incident item : incidentList) {
                        listModel.addElement(item.toString());
                    }
                    listIncident.setModel(listModel);
                }
            }

        });

//        JLabel labelSubs = new JLabel("Objects");
//        labelSubs.setPreferredSize(new Dimension(400 , 30));

        JPanel panelRemoveEdges = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));
        panelRemoveEdges.setPreferredSize(new Dimension(400 , 30));
        panelRemoveEdges.setBackground(new Color(231, 226, 253));
        JLabel labelSubs = new JLabel("Objects");
        panelRemoveEdges.add(labelSubs);

        removeEdge = new JButton("Убрать Object");
        removeEdge.setVisible(false);
        removeEdge.addActionListener(e -> {

            if (listObjOfIncident != null) {
                int index = listObjOfIncident.getSelectedIndex();
                currObjectsList.remove(index);
                if (currObjectIncidentList != null) {
                    currObjectIncidentList.remove(index);
                }


                if (index != -1) {
                    DefaultListModel listModel = new DefaultListModel();
                    for (MyObject en : currObjectsList) {
                        listModel.addElement(en.toString());
                    }

                    listObjOfIncident.setModel(listModel);
                }
            }
        });
        panelRemoveEdges.add(removeEdge);


//        JLabel labelInc = new JLabel("Incidents");
//        labelInc.setPreferredSize(new Dimension(400 , 30));

        JPanel panelRemoveEdgesIncident = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));
        panelRemoveEdgesIncident.setPreferredSize(new Dimension(400 , 30));
        panelRemoveEdgesIncident.setBackground(new Color(231, 226, 253));
        JLabel labelInc = new JLabel("Incidents");
        panelRemoveEdgesIncident.add(labelInc);

        removeEdgeIncident = new JButton("Убрать Incident");
        removeEdgeIncident.setVisible(false);
        removeEdgeIncident.addActionListener(e -> {

            if (listIncOfInc != null) {
                int index = listIncOfInc.getSelectedIndex();
                currIncidentsList.remove(index);
                if (currIncidentIncidentList != null) {
                    currIncidentIncidentList.remove(index);
                }

                if (index != -1) {
                    DefaultListModel listModel = new DefaultListModel();
                    for (Incident en : currIncidentsList) {
                        listModel.addElement(en.toString());
                    }

                    listIncOfInc.setModel(listModel);
                }
            }
        });
        panelRemoveEdgesIncident.add(removeEdgeIncident);


        panelEditFields.add(codeLabel);
        panelEditFields.add(codeEdt);
        panelEditFields.add(messageLabel);
        panelEditFields.add(messageEdt);
        panelEditFields.add(severityLabel);
        panelEditFields.add(severityEdt);
        panelEditFields.add(typeLabel);
        panelEditFields.add(typeEdt);
        panelEditFields.add(eventLabel);
        panelEditFields.add(eventEdt);
        panelEditFields.add(actionLabel);
        panelEditFields.add(actionEdt);
        panelEditFields.add(osiLabel);
        panelEditFields.add(osiEdt);
        panelEditFields.add(commentLabel);
        panelEditFields.add(commentEdt);
        panelEditFields.add(panelRemoveEdges);
        panelEditFields.add(panelObjectOfIncident);
        panelEditFields.add(panelRemoveEdgesIncident);
        panelEditFields.add(panelIncidentOfIncident);
        panelEditFields.add(panelChangeDelete);
        frame.getContentPane().add(panelEditFields, BorderLayout.CENTER);
    }

    protected void initRightPanel(JFrame frame) {
        panelListObject = new JPanel();
        panelListObject.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 10));
        panelListObject.setBackground(new Color(231, 226, 253));
        panelListObject.setPreferredSize(new Dimension(200, 700));

        objectsList = Parser.getAllObjects();
//        listObject = new JList(objectsList.toArray());
//        listObject.setPreferredSize(new Dimension(200, 600));

        JScrollPane scrollPane = new JScrollPane();
        listObject = new JList(objectsList.toArray());
        scrollPane.setViewportView(listObject);
        scrollPane.setPreferredSize(new Dimension(200, 600));



        panelListObject.add(new JLabel("Objects"));
        panelListObject.add(scrollPane);

        JButton button = new JButton("Добавить к Incident");
        panelListObject.add(button);
        button.addActionListener(e->{
            int index = listObject.getSelectedIndex();
            MyObject currObj = objectsList.get(index);

            if (currObjectsList == null) currObjectsList = new ArrayList<>();

            for (MyObject mo : currObjectsList) {
                if (mo.getId() == currObj.getId()){
                    return;
                }
            }

            JTextField commentEdt = new JTextField();
            JCheckBox isSuedByCb = new JCheckBox();
            JCheckBox affectCb = new JCheckBox();
            final JComponent[] inputs = new JComponent[] {
                    new JLabel("Comment"),
                    commentEdt,
                    new JLabel("isSuedBy"),
                    isSuedByCb,
                    new JLabel("affect"),
                    affectCb
            };

            int res = JOptionPane.showConfirmDialog(null, inputs, "Параметры edge", JOptionPane.PLAIN_MESSAGE);

            String strComment = commentEdt.getText();
            if (res == JOptionPane.OK_OPTION && !strComment.isEmpty()) {
                boolean isSuedBy = isSuedByCb.isSelected();
                boolean isAffect = affectCb.isSelected();

                ObjectIncident objInc = new ObjectIncident(currObj.getId(), isSuedBy, isAffect, strComment);
                if (currObjectIncidentList == null) currObjectIncidentList = new ArrayList<>();
                currObjectIncidentList.add(objInc);

                currObjectsList.add(currObj);

                DefaultListModel listModel = new DefaultListModel();
                for (MyObject mo : currObjectsList) {
                    listModel.addElement(mo.toString());
                }

                if (listObjOfIncident != null) {
                    listObjOfIncident.setModel(listModel);
                } else {
                    listObjOfIncident = new JList(listModel);
                    listObjOfIncident.setPreferredSize(new Dimension(400, 100));
                    panelObjectOfIncident.add(listObjOfIncident);
                }

                panelObjectOfIncident.revalidate();
                panelObjectOfIncident.repaint();
            }

        });


        frame.getContentPane().add(panelListObject, BorderLayout.EAST);
    }


    protected void initBottomPanel (JFrame frame) {
        panelButtom = new JPanel();
        panelButtom.setLayout(new FlowLayout());
        panelButtom.setBackground(new Color(231, 226, 253));
        panelButtom.setPreferredSize(new Dimension(700, 50));

        messageLabelBottom = new JLabel();
        panelButtom.add(messageLabelBottom);
        frame.add(panelButtom, BorderLayout.SOUTH);


    }


    protected void clearAll() {
        listIncident.clearSelection();
        codeEdt.setText("");
        commentEdt.setText("");
        severityEdt.setText("");
        typeEdt.setText("");
        messageEdt.setText("");
        eventEdt.setText("");
        osiEdt.setText("");
        actionEdt.setText("");
        currObjectIncidentList = null;
        currIncidentIncidentList = null;
        currObjectsList = null;
        currIncidentsList = null;
        removeEdgeIncident.setVisible(false);
        removeEdge.setVisible(false);
        initObjectsByIncident(null);
        initIncidentsByIncident(null);
    }

    protected void initObjectsByIncident (Incident incident) {

        panelObjectOfIncident.removeAll();
        if (currObjectsList != null) currObjectsList.clear();


        if (incident != null) {
            currObjectsList = Parser.getObjectsByIncident(incident);
            listObjOfIncident = new JList(currObjectsList.toArray());
        } else {
            listObjOfIncident = new JList(new String[1]);
        }

        listObjOfIncident.setPreferredSize(new Dimension(400, 100));
        panelObjectOfIncident.add(listObjOfIncident);


        listObjOfIncident.addListSelectionListener(e -> {
            removeEdge.setVisible(true);
        });

        panelObjectOfIncident.revalidate();
        panelObjectOfIncident.repaint();
    }

    protected void initIncidentsByIncident (Incident incident) {

        panelIncidentOfIncident.removeAll();
        if (currIncidentsList != null) currIncidentsList.clear();


        if (incident != null) {
            currIncidentsList = Parser.getIncidentsByIncident(incident);
            listIncOfInc = new JList(currIncidentsList.toArray());
        } else {
            listIncOfInc = new JList(new String[1]);
        }

        listIncOfInc.setPreferredSize(new Dimension(400, 100));
        panelIncidentOfIncident.add(listIncOfInc);


        listIncOfInc.addListSelectionListener(e -> {
            removeEdgeIncident.setVisible(true);
        });

        panelIncidentOfIncident.revalidate();
        panelIncidentOfIncident.repaint();
    }


}
