package main.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;

public class MyFrame  extends JFrame{

    public MyFrame(String title) throws HeadlessException {
        super(title);
    }

    public void paint(Graphics g) {
        super.paint(g);  // fixes the immediate problem.
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(3));
        g2.setColor(new Color(246, 228, 0));
        Line2D lin1 = new Line2D.Float(350, 100, 350, 148);
        Line2D lin1_1 = new Line2D.Float(350, 148, 340, 138);
        Line2D lin1_2 = new Line2D.Float(350, 148, 360, 138);


        Line2D lin2 = new Line2D.Float(500, 100, 500, 148);
        Line2D lin2_1 = new Line2D.Float(500, 100, 490, 110);
        Line2D lin2_2 = new Line2D.Float(500, 100, 510, 110);

        Line2D lin3 = new Line2D.Float(425, 200, 425, 248);
        Line2D lin3_1 = new Line2D.Float(425, 248, 415, 238);
        Line2D lin3_2 = new Line2D.Float(425, 248, 435, 238);

        Line2D lin4 = new Line2D.Float(425, 300, 425, 348);
        Line2D lin4_1 = new Line2D.Float(425, 348, 415, 338);
        Line2D lin4_2 = new Line2D.Float(425, 348, 435, 338);

        Line2D lin5 = new Line2D.Float(425, 400, 425, 448);
        Line2D lin5_1 = new Line2D.Float(425, 448, 415, 438);
        Line2D lin5_2 = new Line2D.Float(425, 448, 435, 438);

        Line2D lin6 = new Line2D.Float(425, 500, 425, 548);
        Line2D lin6_1 = new Line2D.Float(425, 548, 415, 538);
        Line2D lin6_2 = new Line2D.Float(425, 548, 435, 538);

//        lin.setS
        g2.draw(lin1);
        g2.draw(lin1_1);
        g2.draw(lin1_2);

        g2.draw(lin2);
        g2.draw(lin2_1);
        g2.draw(lin2_2);

        g2.draw(lin3);
        g2.draw(lin3_1);
        g2.draw(lin3_2);

        g2.draw(lin4);
        g2.draw(lin4_1);
        g2.draw(lin4_2);

        g2.draw(lin5);
        g2.draw(lin5_1);
        g2.draw(lin5_2);

        g2.draw(lin6);
        g2.draw(lin6_1);
        g2.draw(lin6_2);
    }

}
