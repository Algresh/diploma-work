package main.gui;


import javax.swing.*;
import java.awt.*;

public class SettingGUI {

    private JFrame frame;

    public void initGUI(){
        frame = new MyFrame("Настройка");
        frame.setSize(250, 350);
        frame.setVisible(true);
        frame.setLayout(new BorderLayout());
        frame.setBackground(new Color(231, 226, 253));

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 15));
        mainPanel.setPreferredSize(new Dimension(250, 350));
        mainPanel.setBackground(new Color(231, 226, 253));

        JLabel nameLabel = new JLabel("Путь к файлу");
        nameLabel.setPreferredSize(new Dimension(200, 20));

        JTextField nameEdt = new JTextField();
        nameEdt.setPreferredSize(new Dimension(200, 30));

//        JLabel tagLabel = new JLabel("Название корневого тега");
//        tagLabel.setPreferredSize(new Dimension(200, 20));
//
//        JTextField tagEdt = new JTextField();
//        tagEdt.setPreferredSize(new Dimension(200, 30));

        JButton btnEnter1 = new JButton("Сохранить");
        btnEnter1.setPreferredSize(new Dimension(200, 50));
        mainPanel.add(btnEnter1);
        btnEnter1.addActionListener(e -> {
            EnterpriseGUI enterpriseGUI = new EnterpriseGUI();
            enterpriseGUI.initGUI();
        });

        JLabel text = new JLabel("<html>" +
                "Если путь XML файлу не указан то по умолчанию считается файл находящийся в той же папке с программой с названием graph.xml." +
                "</html>");
        text.setPreferredSize(new Dimension(200, 100));

        mainPanel.add(nameLabel);
        mainPanel.add(nameEdt);
//        mainPanel.add(tagLabel);
//        mainPanel.add(tagEdt);
        mainPanel.add(btnEnter1);
        mainPanel.add(text);

        frame.add(mainPanel);

    }
}
