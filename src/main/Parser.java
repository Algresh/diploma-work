package main;

import main.dao.*;
import main.dom.DOMParser;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Parser {


    private final static String PATH = "/home/alex/IdeaProjects/parser/src/main/test.xml";

//    public static void main(String[] args) {
//
//        File input = new File(PATH);
//
//        try {
//            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
//                    .newInstance();
//            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
//            Document doc = dBuilder.parse(input);
//            doc.getDocumentElement().normalize();
//            DOMParser domParser = new DOMParser();
//
//
//            System.out.println("getSubsystemsByEnterprise -----------");
//            List<Subsystem> subsystems = domParser.getSubsystemsByEnterprise(doc, new Enterprise(2));
//            for (Subsystem s : subsystems) {
//                System.out.println(s);
//            }
//            System.out.println("getEnterprisesBySubsystem -----------");
//            List<Enterprise> enterprises = domParser.getEnterprisesBySubsystem(doc, new Subsystem(14));
//            for (Enterprise e : enterprises) {
//                System.out.println(e);
//            }
//            System.out.println("getAllComponent -----------");
//            List<Component> components = domParser.getAllComponent(doc);
//            for (Component c : components) {
//                System.out.println(c);
//            }
//            System.out.println("getComponentsBySubsystem -----------");
//            List<Component> components2 = domParser.getComponentsBySubsystem(doc, new Subsystem(21));
//            for (Component c : components2) {
//                System.out.println(c);
//            }
//            System.out.println("getAllObjGroups -----------");
//            List<ObjGroup> objGroups = domParser.getAllObjGroups(doc);
//            for (ObjGroup o : objGroups) {
//                System.out.println(o);
//            }
//
//            System.out.println("getsObjGroupByComponent -----------");
//            List<ObjGroup> objGroups2 = domParser.getsObjGroupByComponent(doc, new Component(212));
//            for (ObjGroup o : objGroups2) {
//                System.out.println(o);
//            }
//            System.out.println("getObjectsByObjGroup -----------");
//            List<MyObject> myObjectList = domParser.getObjectsByObjGroup(doc, new ObjGroup(1111));
//            for (MyObject o : myObjectList) {
//                System.out.println(o);
//            }
//            System.out.println("getAllIncidents -----------");
//            List<Incident> incidents = domParser.getAllIncidents(doc);
//            for (Incident o : incidents) {
//                System.out.println(o);
//            }
//            System.out.println("getAllIncidents -----------");
//            List<Incident> incidents2 = domParser.getIncidentsByObject(doc, new MyObject(21411));
//            for (Incident o : incidents2) {
//                System.out.println(o);
//            }
//            System.out.println("addEnterprise -----------");
//
////            List<Subsystem> listSub = new ArrayList<>();
////            listSub.add(new Subsystem(14));
//
//            boolean b = false;
////            boolean b = domParser.deleteIncident(doc, new Incident(214119));
////            boolean b = domParser.changeIncident(doc, new Incident(214119, "111", "AAA", "BBBB", "CCCC", "DDDDDD", "EEEEE", 2, "FFFF"));
////            boolean b = domParser.addIncident(doc, new Incident("sda", "asdasd", "asdasd", "asdasd", "asdasd", "asdasd", 1, "asdasd"),
////                    new ObjectIncident(21411, false, true, "sadda"));
////            boolean b = domParser.changeObject(doc, new MyObject(21411, "UUUUUUUUU", "ZZZZZZZZZZzz"));
////            boolean b = domParser.changeObjGroup(doc, new ObjGroup(2141, "hih44444ihih", "gKKKg"));
////            boolean b = domParser.addObjGroup(doc, new ObjGroup("hihihih", "gg") ,new Component(214, "545454", "sssss", "88888"));
////            boolean b = domParser.changeComponent(doc, new Component(214, "545454", "sssss", "88888"));
////            boolean b = domParser.addComponent(doc, new Component(417, "sdfsdddd", "aaaa", "tttttt"), new Subsystem(21), true);
////            boolean b = domParser.changeSubsystem(doc, new Subsystem(21, "NNNNNN", "PPPPPP"));
////            boolean b = domParser.changeEnterprise(doc, new Enterprise(1, "TTTTT222TT", "SSS222SSSS"));
////            boolean b = domParser.addSubsystem(doc,new Subsystem("alex4", "ttt4"), new Enterprise(2, "gjgjjg", "asdasd"));
////            boolean b = domParser.addEnterprise(doc, new Enterprise(2, "gjgjjg", "asdasd"));
//            System.out.print(b);
//
//
//            if (b) {
//                TransformerFactory transformerFactory = TransformerFactory.newInstance();
//                Transformer transformer = transformerFactory.newTransformer();
//                DOMSource domSource = new DOMSource(doc);
//                StreamResult streamResult = new StreamResult(input);
//                transformer.transform(domSource, streamResult);
//            }
//
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }

    public static Document doc;
    public static File input;

    public static List<Enterprise> getAllEnterprises(){
        input = new File(PATH);
        List<Enterprise> enterprises = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(input);
            doc.getDocumentElement().normalize();
            DOMParser domParser = new DOMParser();


            enterprises = domParser.getAllEnterprises(doc);
        }catch (Exception e) {}

        return enterprises;
    }

    public static List<Subsystem> getAllSubsystems(){
        input = new File(PATH);
        List<Subsystem> subsystems = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(input);
            doc.getDocumentElement().normalize();
            DOMParser domParser = new DOMParser();


            subsystems = domParser.getAllSubsystems(doc);
        }catch (Exception e) {}

        return subsystems;
    }

    public static List<Subsystem> getSubsystemsByEnterprise(Enterprise enterprise){
        input = new File(PATH);
        List<Subsystem> subsystems = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(input);
            doc.getDocumentElement().normalize();
            DOMParser domParser = new DOMParser();


            subsystems = domParser.getSubsystemsByEnterprise(doc, enterprise);
        } catch (Exception e) {}

        return subsystems;
    }

    public static boolean changeEnterprise(Enterprise enterprise, List<Subsystem> subOfEnter){

        try {
            DOMParser domParser = new DOMParser();
            boolean b = domParser.changeEnterprise(doc, enterprise, subOfEnter);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return true;

    }

    public static boolean deleteEnterprise(Enterprise enterprise){

        try {
            DOMParser domParser = new DOMParser();
            boolean b = domParser.deleteEnterprise(doc, enterprise);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return true;

    }

    public static boolean addEnterprise(Enterprise enterprise){

        try {
            DOMParser domParser = new DOMParser();
            boolean b = domParser.addEnterprise(doc, enterprise);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return true;

    }

    public static boolean addEnterprise(Enterprise enterprise, List<Subsystem> subsystemList){

        try {
            DOMParser domParser = new DOMParser();
            boolean b = domParser.addEnterprise(doc, enterprise, subsystemList);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return true;

    }


    //--------------------------------------------------------------------

    public static List<Enterprise> getEnterprisesBySubsystem(Subsystem subsystem){
        input = new File(PATH);
        List<Enterprise> enterprises = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(input);
            doc.getDocumentElement().normalize();
            DOMParser domParser = new DOMParser();


            enterprises = domParser.getEnterprisesBySubsystem(doc, subsystem);
        } catch (Exception e) {}

        return enterprises;
    }

    public static boolean changeSubsystem (Subsystem subsystem, List<Enterprise> subOfEnter){

        try {
            DOMParser domParser = new DOMParser();
            boolean b = domParser.changeSubsystem(doc, subsystem, subOfEnter);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return true;

    }

    public static boolean deleteSubsystem (Subsystem subsystem){

        try {
            DOMParser domParser = new DOMParser();
            boolean b = domParser.deleteSubsystem(doc, subsystem);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return true;
    }

    public static Subsystem addSubsystem (Subsystem subsystem, List<Enterprise> enterprises){

        Subsystem sub = null;
        try {
            DOMParser domParser = new DOMParser();
            sub = domParser.addSubsystem(doc, subsystem, enterprises);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return sub;

    }

    //-----------------------------------COMPONENT----------------------------------------------

    public static List<Component> getAllComponents() {
        input = new File(PATH);
        List<Component> components = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(input);
            doc.getDocumentElement().normalize();
            DOMParser domParser = new DOMParser();


            components = domParser.getAllComponent(doc);
        }catch (Exception e) {}

        return components;
    }

    public static List<Subsystem> getSubsystemByComponent (Component component){
        input = new File(PATH);
        List<Subsystem> subsystems = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(input);
            doc.getDocumentElement().normalize();
            DOMParser domParser = new DOMParser();


            subsystems = domParser.getSubsystemByComponent(doc, component);
        } catch (Exception e) {}

        return subsystems;
    }

    public static boolean changeComponent (Component component, Subsystem subOfCom, boolean isnative){

        try {
            DOMParser domParser = new DOMParser();
            boolean b = domParser.changeComponent(doc, component, subOfCom, isnative);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return true;
    }

    public static boolean deleteComponent (Component component){

        try {
            DOMParser domParser = new DOMParser();
            boolean b = domParser.deleteComponent(doc, component);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return true;
    }

    public static Component addComponent (Component component, Subsystem subsystem, boolean isnative){

        Component comp = null;
        try {
            DOMParser domParser = new DOMParser();
            comp = domParser.addComponent(doc, component, subsystem, isnative);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return comp;

    }
//-----------------------------------ObjGroup----------------------------------------------


    public static List<ObjGroup> getAllObjGroups () {
        input = new File(PATH);
        List<ObjGroup> objGroups = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(input);
            doc.getDocumentElement().normalize();
            DOMParser domParser = new DOMParser();


            objGroups = domParser.getAllObjGroups(doc);
        }catch (Exception e) {}

        return objGroups;
    }

    public static List<Component> getComponentByObjGroups (ObjGroup objGroup){
        input = new File(PATH);
        List<Component> components = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(input);
            doc.getDocumentElement().normalize();
            DOMParser domParser = new DOMParser();


            components = domParser.getComponentsByObjGroup(doc, objGroup);
        } catch (Exception e) {}

        return components;
    }

    public static boolean changeObjGroup (ObjGroup objGroup, Component comOfObjGrp){

        try {
            DOMParser domParser = new DOMParser();
            boolean b = domParser.changeObjGroup(doc, objGroup, comOfObjGrp);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return true;
    }

    public static boolean deleteObjGroup (ObjGroup objGroup){

        try {
            DOMParser domParser = new DOMParser();
            boolean b = domParser.deleteObjectGroup(doc, objGroup);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return true;
    }

    public static ObjGroup addObjGroup (ObjGroup objGroup, Component component){

        ObjGroup obGr = null;
        try {
            DOMParser domParser = new DOMParser();
            obGr = domParser.addObjGroup(doc, objGroup, component);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return obGr;

    }


    //-----------------------------------Object----------------------------------------------


    public static List<MyObject> getAllObjects () {
        input = new File(PATH);
        List<MyObject> myObjectList = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(input);
            doc.getDocumentElement().normalize();
            DOMParser domParser = new DOMParser();


            myObjectList = domParser.getAllObjects(doc);
        }catch (Exception e) {}

        return myObjectList;
    }

    public static List<ObjGroup> getObjGroupByObject (MyObject myObject){
        input = new File(PATH);
        List<ObjGroup> objGroups = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(input);
            doc.getDocumentElement().normalize();
            DOMParser domParser = new DOMParser();


            objGroups = domParser.getObjGroupsByObject(doc, myObject);
        } catch (Exception e) {}

        return objGroups;
    }

    public static boolean changeObject (MyObject myObject, ObjGroup objGrpOfObj){

        try {
            DOMParser domParser = new DOMParser();
            boolean b = domParser.changeObject(doc, myObject, objGrpOfObj);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return true;
    }

    public static boolean deleteObject (MyObject myObject){

        try {
            DOMParser domParser = new DOMParser();
            boolean b = domParser.deleteObject(doc, myObject);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return true;
    }

    public static MyObject addObject (MyObject myObject, ObjGroup objGroup){

        MyObject obj = null;
        try {
            DOMParser domParser = new DOMParser();
            obj = domParser.addObject(doc,myObject, objGroup);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return obj;

    }

    //----------------------------------- Incident ----------------------------------------------


    public static List<Incident> getAllIncidents () {
        input = new File(PATH);
        List<Incident> incidents = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(input);
            doc.getDocumentElement().normalize();
            DOMParser domParser = new DOMParser();


            incidents = domParser.getAllIncidents(doc);
        }catch (Exception e) {}

        return incidents;
    }

    public static List<MyObject> getObjectsByIncident (Incident incident){
        input = new File(PATH);
        List<MyObject> myObjectList = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(input);
            doc.getDocumentElement().normalize();
            DOMParser domParser = new DOMParser();


            myObjectList = domParser.getObjectsByIncident(doc, incident);
        } catch (Exception e) {}

        return myObjectList;
    }

    public static List<Incident> getIncidentsByIncident (Incident incident){
        input = new File(PATH);
        List<Incident> incidentList = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(input);
            doc.getDocumentElement().normalize();
            DOMParser domParser = new DOMParser();


            incidentList = domParser.getIncidentsByIncident(doc, incident);
        } catch (Exception e) {}

        return incidentList;
    }

    public static boolean changeIncident (Incident incident, List<ObjectIncident> objectIncidentList, List<IncidentIncident> incidentIncidentList,  List<MyObject> myObjectList, List<Incident> incidentList){

        try {
            DOMParser domParser = new DOMParser();
            boolean b = domParser.changeIncident(doc, incident, objectIncidentList, incidentIncidentList, myObjectList, incidentList);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return true;
    }

    public static boolean deleteIncident (Incident incident){

        try {
            DOMParser domParser = new DOMParser();
            boolean b = domParser.deleteIncident(doc, incident);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return true;
    }

    public static Incident addIncident (Incident incident, List<ObjectIncident> objectIncidents, List<IncidentIncident> incidentIncidents){

        Incident inc = null;
        try {
            DOMParser domParser = new DOMParser();
            inc = domParser.addIncident(doc,incident, objectIncidents, incidentIncidents);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(input);
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return inc;
    }

//    public static Incident addIncidentIncInc (Incident incident, List<IncidentIncident> incidentIncidents){
//
//        Incident inc = null;
//        try {
//            DOMParser domParser = new DOMParser();
//            inc = domParser.addIncidentIncInc(doc,incident, incidentIncidents);
//
//            TransformerFactory transformerFactory = TransformerFactory.newInstance();
//            Transformer transformer = transformerFactory.newTransformer();
//            DOMSource domSource = new DOMSource(doc);
//            StreamResult streamResult = new StreamResult(input);
//            transformer.transform(domSource, streamResult);
//        } catch (TransformerException e) {
//            e.printStackTrace();
//        }
//
//        return inc;
//    }
}
