package main.dom;

import main.dao.*;
import org.w3c.dom.*;

import java.util.*;

public class DOMParser {

//---------------------------------------- DELETE ----------------------------------------------------

    public boolean deleteEnterprise (Document input, Enterprise enterprise) {
        Node node = getEnterpriseNodeById(input, String.valueOf(enterprise.getId()));
        if (node == null) {
            return false;
        }

        NodeList propertyChildrenNotes = node.getChildNodes();

        for (int i = 0; i < propertyChildrenNotes.getLength(); i++) {
            node.removeChild(propertyChildrenNotes.item(i));
        }

        Node parentNode = node.getParentNode();
        parentNode.removeChild(node);

        List<Node> edgesBetweenEnerSub = getEdgeBetweenEnterSub(input, enterprise);
        for (Node item : edgesBetweenEnerSub) {
            parentNode.removeChild(item);
        }

        return true;

    }

    public boolean deleteSubsystem (Document input, Subsystem subsystem) {
        Node node = getSubsystemNodeById(input, String.valueOf(subsystem.getId()));
        if (node == null) {
            return false;
        }

        Node parentNode = node.getParentNode();
        parentNode.removeChild(node);

        List<Node> edgesSubsystem = getEdgeSubsystem(input, subsystem);
        for (Node item : edgesSubsystem) {
            parentNode.removeChild(item);
        }

        return true;

    }

    public boolean deleteComponent (Document input, Component component) {
        Node node = getComponentNodeById(input, String.valueOf(component.getId()));
        if (node == null) {
            return false;
        }

        Node parentNode = node.getParentNode();
        parentNode.removeChild(node);

        List<Node> edgesComponent = getEdgeComponent(input, component);
        for (Node item : edgesComponent) {
            parentNode.removeChild(item);
        }

        return true;
    }

    public boolean deleteObjectGroup (Document input, ObjGroup objGroup) {
        Node node = getObjGroupNodeById(input, String.valueOf(objGroup.getId()));
        if (node == null) {
            return false;
        }

        Node parentNode = node.getParentNode();
        parentNode.removeChild(node);

        List<Node> edgesObjectGroup = getEdgeObjectGroup(input, objGroup);
        for (Node item : edgesObjectGroup) {
            parentNode.removeChild(item);
        }

        return true;
    }

    public boolean deleteObject (Document input, MyObject myObject) {
        Node node = getObjectNodeById(input, String.valueOf(myObject.getId()));
        if (node == null) {
            return false;
        }

        Node parentNode = node.getParentNode();
        parentNode.removeChild(node);

        List<Node> edgesObject = getEdgeObject(input, myObject);
        for (Node item : edgesObject) {
            parentNode.removeChild(item);
        }

        return true;
    }

    public boolean deleteIncident (Document input, Incident incident) {
        Node node = getIncidentNodeById(input, String.valueOf(incident.getId()));
        if (node == null) {
            return false;
        }

        Node parentNode = node.getParentNode();
        parentNode.removeChild(node);

        List<Node> edgesIncident= getEdgeIncident(input, incident);
        for (Node item : edgesIncident) {
            parentNode.removeChild(item);
        }

        return true;
    }

    //-------------------------------------- CHANGE -------------------------------------------------------
    public boolean changeEnterprise (Document input, Enterprise enterprise, List<Subsystem> subOfEnter) {

        Node node = getEnterpriseNodeById(input, String.valueOf(enterprise.getId()));
        if (node == null) {
            return false;
        }

        NodeList listChildren = node.getChildNodes();

        for (int i = 0; i < listChildren.getLength(); i++) {
            Node child = listChildren.item(i);
            if (child.getNodeName().equals("property")) {
                NamedNodeMap childAttr = child.getAttributes();
                String name = childAttr.getNamedItem("name").getNodeValue();

                if (name.equals("enterprisename")) {
                    childAttr.getNamedItem("value").setTextContent(enterprise.getEnterpriseName());
                }
                if (name.equals("comment")) {
                    childAttr.getNamedItem("value").setTextContent(enterprise.getComment());
                }
            }
        }

        Node rootNode = input.getDocumentElement();
        List<Node> edgesBetweenEnerSub = getEdgeBetweenEnterSub(input, enterprise);

        for (Node oldNode : edgesBetweenEnerSub) {
            rootNode.removeChild(oldNode);
        }

        if (subOfEnter != null) {
            for (Subsystem item : subOfEnter) {
                addEdgeBetweenEnterAndSub(input, enterprise, item);
            }
        }
        return true;
    }

    public boolean changeSubsystem (Document input, Subsystem subsystem, List<Enterprise> subOfEnter) {

        Node node = getSubsystemNodeById(input, String.valueOf(subsystem.getId()));
        if (node == null) {
            return false;
        }

        NodeList listChildren = node.getChildNodes();

        for (int i = 0; i < listChildren.getLength(); i++) {
            Node child = listChildren.item(i);
            if (child.getNodeName().equals("property")) {
                NamedNodeMap childAttr = child.getAttributes();
                String name = childAttr.getNamedItem("name").getNodeValue();

                if (name.equals("subname")) {
                    childAttr.getNamedItem("value").setTextContent(subsystem.getSubsystemName());
                }
                if (name.equals("comment")) {
                    childAttr.getNamedItem("value").setTextContent(subsystem.getComment());
                }
            }
        }

        Node rootNode = input.getDocumentElement();
        List<Node> edges = getEdgeSubsystemSingle(input, subsystem);

        for (Node oldNode : edges) {
            rootNode.removeChild(oldNode);
        }

        if (subOfEnter != null) {
            for (Enterprise item : subOfEnter) {
                addEdgeBetweenEnterAndSub(input, item, subsystem);
            }
        }

        return true;
    }

    public boolean changeComponent (Document input, Component component, Subsystem subOfCom, boolean isnative) {

        Node node = getComponentNodeById(input, String.valueOf(component.getId()));
        if (node == null) {
            return false;
        }

        NodeList listChildren = node.getChildNodes();

        for (int i = 0; i < listChildren.getLength(); i++) {
            Node child = listChildren.item(i);
            if (child.getNodeName().equals("property")) {
                NamedNodeMap childAttr = child.getAttributes();
                String name = childAttr.getNamedItem("name").getNodeValue();

                if (name.equals("compname")) {
                    childAttr.getNamedItem("value").setTextContent(component.getCompName());
                }
                if (name.equals("comment")) {
                    childAttr.getNamedItem("value").setTextContent(component.getComment());
                }
                if (name.equals("vendor")) {
                    childAttr.getNamedItem("value").setTextContent(component.getVendor());
                }
            }
        }

        Node rootNode = input.getDocumentElement();
        List<Node> edges = getEdgeComponentSingle(input, component);

        for (Node oldNode : edges) {
            rootNode.removeChild(oldNode);
        }

        if (subOfCom != null) {
            addEdgeBetweenSubAndComp(input, component, subOfCom, isnative);
        }


        return true;
    }

    public boolean changeObjGroup (Document input, ObjGroup objGroup, Component comOfObjGrp) {

        Node node = getObjGroupNodeById(input, String.valueOf(objGroup.getId()));
        if (node == null) {
            return false;
        }

        NodeList listChildren = node.getChildNodes();

        for (int i = 0; i < listChildren.getLength(); i++) {
            Node child = listChildren.item(i);
            if (child.getNodeName().equals("property")) {
                NamedNodeMap childAttr = child.getAttributes();
                String name = childAttr.getNamedItem("name").getNodeValue();

                if (name.equals("objgname")) {
                    childAttr.getNamedItem("value").setTextContent(objGroup.getObjgname());
                }
                if (name.equals("belongsto")) {
                    childAttr.getNamedItem("value").setTextContent(objGroup.getBelongsto());
                }
            }
        }

        Node rootNode = input.getDocumentElement();
        List<Node> edges = getEdgeObjectGroupSingle(input, objGroup);

        for (Node oldNode : edges) {
            rootNode.removeChild(oldNode);
        }

        if (comOfObjGrp != null) {
            addEdgeBetweenCompAndObjGrp(input, comOfObjGrp, objGroup);
        }


        return true;
    }

    public boolean changeObject(Document input, MyObject myObject, ObjGroup objGrpOfObj) {

        Node node = getObjectNodeById(input, String.valueOf(myObject.getId()));
        if (node == null) {
            return false;
        }

        NodeList listChildren = node.getChildNodes();

        for (int i = 0; i < listChildren.getLength(); i++) {
            Node child = listChildren.item(i);
            if (child.getNodeName().equals("property")) {
                NamedNodeMap childAttr = child.getAttributes();
                String name = childAttr.getNamedItem("name").getNodeValue();

                if (name.equals("objname")) {
                    childAttr.getNamedItem("value").setTextContent(myObject.getObjname());
                }
                if (name.equals("belongsto")) {
                    childAttr.getNamedItem("value").setTextContent(myObject.getBelongsto());
                }
            }
        }

        Node rootNode = input.getDocumentElement();
        List<Node> edges = getEdgeObjectSingle(input, myObject);

        for (Node oldNode : edges) {
            rootNode.removeChild(oldNode);
        }

        if (objGrpOfObj != null) {
            addEdgeBetweenObjGrpAndObj(input, objGrpOfObj, myObject);
        }

        return true;
    }

    public boolean changeIncident(Document input, Incident incident, List<ObjectIncident> objectIncidentList,
                                  List<IncidentIncident> incidentIncidentList,
                                  List<MyObject> myObjectList, List<Incident> incidentList
    ) {

        Node node = getIncidentNodeById(input, String.valueOf(incident.getId()));
        if (node == null) {
            return false;
        }

        NodeList listChildren = node.getChildNodes();

        for (int i = 0; i < listChildren.getLength(); i++) {
            Node child = listChildren.item(i);
            if (child.getNodeName().equals("property")) {
                NamedNodeMap childAttr = child.getAttributes();
                String name = childAttr.getNamedItem("name").getNodeValue();


                if (name.equals("code")) {
                    childAttr.getNamedItem("value").setTextContent(incident.getCode());
                }

                if (name.equals("message")) {
                    childAttr.getNamedItem("value").setTextContent(incident.getMessage());
                }

                if (name.equals("severity")) {
                    childAttr.getNamedItem("value").setTextContent(incident.getSeverity());
                }

                if (name.equals("type")) {
                    childAttr.getNamedItem("value").setTextContent(incident.getType());
                }

                if (name.equals("event")) {
                    childAttr.getNamedItem("value").setTextContent(incident.getEvent());
                }

                if (name.equals("action")) {
                    childAttr.getNamedItem("value").setTextContent(incident.getAction());
                }

                if (name.equals("osi")) {
                    childAttr.getNamedItem("value").setTextContent(String.valueOf(incident.getOsi()));
                }

                if (name.equals("comment")) {
                    childAttr.getNamedItem("value").setTextContent(incident.getComment());
                }
            }
        }

        Node rootNode = input.getDocumentElement();
        List<Node> edgesIncInc = getEdgeIncidentIncident(input, incident);

        for (Node oldNode : edgesIncInc) {
            NamedNodeMap nodeMap = oldNode.getAttributes();
            int fromId = Integer.parseInt(nodeMap.getNamedItem("fromid").getNodeValue());
            int toId = Integer.parseInt(nodeMap.getNamedItem("toid").getNodeValue());

            boolean flagFound = false;
            for (Incident item : incidentList) {
                if (item.getId() == toId || item.getId() == fromId) {
                    flagFound = true;
                    break;
                }
            }

            if (!flagFound) {
                rootNode.removeChild(oldNode);
            }
        }

        if (incidentIncidentList != null) {
            addEdgeBetweenIncAndInc(input, incident, incidentIncidentList);
        }

        List<Node> edgesObjInc = getEdgeObjectIncident(input, incident);
        for (Node oldNode : edgesObjInc) {
            NamedNodeMap nodeMap = oldNode.getAttributes();
            int fromId = Integer.parseInt(nodeMap.getNamedItem("fromid").getNodeValue());
//            int toId = Integer.parseInt(nodeMap.getNamedItem("toid").getNodeValue());

            boolean flagFound = false;
            for (MyObject item : myObjectList) {
                if (item.getId() == fromId) {
                    flagFound = true;
                    break;
                }
            }

            if (!flagFound) {
                rootNode.removeChild(oldNode);
            }
        }

        if (objectIncidentList != null) {
            addEdgeBetweenObjAndInc(input, incident, objectIncidentList);
        }



        return true;
    }



    //-------------------------------------- CHANGE END -------------------------------------------------------
    //-------------------------------------- ADD -------------------------------------------------------

    public boolean addEnterprise (Document input, Enterprise enterprise) {

        Node rootNode = input.getDocumentElement();

        List<Enterprise> list = getAllEnterprises(input);
        int numEnterprises = list.size();
        if (numEnterprises == 9) return false;
        numEnterprises++;
        enterprise.setId(numEnterprises);

        Element newEnterprise = input.createElement("node");
        newEnterprise.setAttribute("id", String.valueOf(numEnterprises));
        newEnterprise.setAttribute("type", "ENTERPRISE");

        Element propertyName = input.createElement("property");
        propertyName.setAttribute("name", "enterprisename");
        propertyName.setAttribute("value", enterprise.getEnterpriseName());

        Element propertyComment = input.createElement("property");
        propertyComment.setAttribute("name", "comment");
        propertyComment.setAttribute("value", enterprise.getComment());

        rootNode.appendChild(newEnterprise);
        newEnterprise.appendChild(propertyName);
        newEnterprise.appendChild(propertyComment);


        return true;
    }

    public Subsystem addSubsystem (Document input, Subsystem subsystem, List<Enterprise> enterprises) {

        Node rootNode = input.getDocumentElement();

        if (enterprises.isEmpty()) return null;

        int idSubSystem = getIdSubSystem(input, subsystem, enterprises.get(0));
        if (idSubSystem == -1) {
            return null;
        }
        subsystem.setId(idSubSystem);

        Element newSubSystem = input.createElement("node");
        newSubSystem.setAttribute("id", String.valueOf(idSubSystem));
        newSubSystem.setAttribute("type", "SUBSYSTEM");

        Element propertyName = input.createElement("property");
        propertyName.setAttribute("name", "subname");
        propertyName.setAttribute("value", subsystem.getSubsystemName());

        Element propertyComment = input.createElement("property");
        propertyComment.setAttribute("name", "comment");
        propertyComment.setAttribute("value", subsystem.getComment());

        rootNode.appendChild(newSubSystem);
        newSubSystem.appendChild(propertyName);
        newSubSystem.appendChild(propertyComment);

        for (Enterprise enterprise : enterprises) {
            addEdgeBetweenEnterAndSub(input, enterprise, subsystem);
        }

        return subsystem;

    }


    public Component addComponent (Document input, Component component, Subsystem subsystem, boolean isnative) {

        Node rootNode = input.getDocumentElement();

        int idComponent = getIdComponent(input, component, subsystem);
        if (idComponent == -1) {
            return null;
        }
        component.setId(idComponent);

        Element newSubSystem = input.createElement("node");
        newSubSystem.setAttribute("id", String.valueOf(idComponent));
        newSubSystem.setAttribute("type", "COMPONENT");

        Element propertyName = input.createElement("property");
        propertyName.setAttribute("name", "compname");
        propertyName.setAttribute("value", component.getCompName());

        Element propertyComment = input.createElement("property");
        propertyComment.setAttribute("name", "comment");
        propertyComment.setAttribute("value", component.getComment());

        Element propertyVendor = input.createElement("property");
        propertyVendor.setAttribute("name", "vendor");
        propertyVendor.setAttribute("value", component.getVendor());


        rootNode.appendChild(newSubSystem);
        newSubSystem.appendChild(propertyName);
        newSubSystem.appendChild(propertyComment);
        newSubSystem.appendChild(propertyVendor);

        addEdgeBetweenSubAndComp(input, component, subsystem, isnative);


        return component;

    }


    public ObjGroup addObjGroup (Document input, ObjGroup objGroup, Component component) {

        Node rootNode = input.getDocumentElement();

        int numGrps = getIdObjGroup(input, objGroup, component);
        if (numGrps == -1) {
            return null;
        }
        objGroup.setId(numGrps);

        Element newSubSystem = input.createElement("node");
        newSubSystem.setAttribute("id", String.valueOf(numGrps));
        newSubSystem.setAttribute("type", "OBJGROUP");

        Element propertyName = input.createElement("property");
        propertyName.setAttribute("name", "objgname");
        propertyName.setAttribute("value", objGroup.getObjgname());

        Element propertyComment = input.createElement("property");
        propertyComment.setAttribute("name", "belongsto");
        propertyComment.setAttribute("value", objGroup.getBelongsto());

        rootNode.appendChild(newSubSystem);
        newSubSystem.appendChild(propertyName);
        newSubSystem.appendChild(propertyComment);

        addEdgeBetweenCompAndObjGrp(input, component, objGroup);


        return objGroup;

    }


    public MyObject addObject (Document input, MyObject myObject, ObjGroup objGroup) {

        Node rootNode = input.getDocumentElement();

        int idObject = getIdObject(input, myObject, objGroup);
        if (idObject == -1) {
            return null;
        }
        myObject.setId(idObject);

        Element newSubSystem = input.createElement("node");
        newSubSystem.setAttribute("id", String.valueOf(idObject));
        newSubSystem.setAttribute("type", "OBJECT");

        Element propertyName = input.createElement("property");
        propertyName.setAttribute("name", "objname");
        propertyName.setAttribute("value", myObject.getObjname());

        Element propertyComment = input.createElement("property");
        propertyComment.setAttribute("name", "belongsto");
        propertyComment.setAttribute("value", myObject.getBelongsto());

        rootNode.appendChild(newSubSystem);
        newSubSystem.appendChild(propertyName);
        newSubSystem.appendChild(propertyComment);

        addEdgeBetweenObjGrpAndObj(input, objGroup, myObject);


        return myObject;

    }

    public Incident addIncident (Document input, Incident incident, List<ObjectIncident> objectIncidents, List<IncidentIncident> incidentIncidents) {

        int idIncident;
        if (objectIncidents != null) {
            idIncident = getIdIncident(input, incident, objectIncidents.get(0));
        } else {
            idIncident = getIdIncident(input, incident, null);
        }

        if (idIncident == -1) {
            return null;
        }
        incident.setId(idIncident);

        addIncident(input, incident, idIncident);

        if (objectIncidents != null) {
            addEdgeBetweenObjAndInc(input, incident, objectIncidents);
        }


        if (incidentIncidents != null) {
            addEdgeBetweenIncAndInc(input, incident, incidentIncidents);
        }




        return incident;

    }

//    public Incident addIncidentIncInc (Document input, Incident incident, List<IncidentIncident> incidentIncidents) {
//
//        int idIncident = getIdIncident(input, incident, incidentIncidents.get(0));
//        if (idIncident == -1) {
//            return null;
//        }
//        incident.setId(idIncident);
//
//        addIncident(input, incident, idIncident);
//
//        addEdgeBetweenIncAndInc(input, incident, incidentIncidents);
//
//
//        return incident;
//
//    }

    protected void addIncident (Document input, Incident incident, int numEnterprises) {
        Node rootNode = input.getDocumentElement();

        Element newSubSystem = input.createElement("node");
        newSubSystem.setAttribute("id", String.valueOf(numEnterprises));
        newSubSystem.setAttribute("type", "INCIDENT");

        Element propertyCode = input.createElement("property");
        propertyCode.setAttribute("name", "code");
        propertyCode.setAttribute("value", incident.getCode());

        Element propertyMessage = input.createElement("property");
        propertyMessage.setAttribute("name", "message");
        propertyMessage.setAttribute("value", incident.getMessage());

        Element propertySeverity = input.createElement("property");
        propertySeverity.setAttribute("name", "severity");
        propertySeverity.setAttribute("value", incident.getSeverity());

        Element propertyType = input.createElement("property");
        propertyType.setAttribute("name", "type");
        propertyType.setAttribute("value", incident.getType());

        Element propertyEvent = input.createElement("property");
        propertyEvent.setAttribute("name", "event");
        propertyEvent.setAttribute("value", incident.getEvent());

        Element propertyAction = input.createElement("property");
        propertyAction.setAttribute("name", "action");
        propertyAction.setAttribute("value", incident.getAction());

        Element propertyOsi = input.createElement("property");
        propertyOsi.setAttribute("name", "osi");
        propertyOsi.setAttribute("value", String.valueOf(incident.getOsi()));

        Element propertyComment = input.createElement("property");
        propertyComment.setAttribute("name", "comment");
        propertyComment.setAttribute("value", incident.getComment());


        rootNode.appendChild(newSubSystem);
        newSubSystem.appendChild(propertyCode);
        newSubSystem.appendChild(propertyMessage);
        newSubSystem.appendChild(propertySeverity);
        newSubSystem.appendChild(propertyType);
        newSubSystem.appendChild(propertyEvent);
        newSubSystem.appendChild(propertyAction);
        newSubSystem.appendChild(propertyOsi);
        newSubSystem.appendChild(propertyComment);
    }

    public boolean addEnterprise (Document input, Enterprise enterprise, List<Subsystem> subsystemList) {

        boolean success = addEnterprise(input, enterprise);

        if (!success) {
            return false;
        }

        for (Subsystem item : subsystemList) {
            addEdgeBetweenEnterAndSub(input, enterprise, item);
        }

        return true;
    }

    public void addEdgeBetweenEnterAndSub(Document input, Enterprise enterprise, Subsystem subsystem) {
        Node rootNode = input.getDocumentElement();
        Element newEdge = input.createElement("edge");
        newEdge.setAttribute("type", "ENTERPRISE_SUBSYSTEM");
        newEdge.setAttribute("fromid", String.valueOf(enterprise.getId()));
        newEdge.setAttribute("toid", String.valueOf(subsystem.getId()));
        rootNode.appendChild(newEdge);
    }

    public void addEdgeBetweenCompAndObjGrp(Document input, Component component, ObjGroup objGroup) {
        Node rootNode = input.getDocumentElement();
        Element newEdge = input.createElement("edge");
        newEdge.setAttribute("type", "COMPONENT_OBJGROUP");
        newEdge.setAttribute("fromid", String.valueOf(component.getId()));
        newEdge.setAttribute("toid", String.valueOf(objGroup.getId()));
        rootNode.appendChild(newEdge);
    }

    public void addEdgeBetweenObjGrpAndObj(Document input, ObjGroup objGroup, MyObject myObject) {
        Node rootNode = input.getDocumentElement();
        Element newEdge = input.createElement("edge");
        newEdge.setAttribute("type", "OBJGROUP_OBJECT");
        newEdge.setAttribute("fromid", String.valueOf(objGroup.getId()));
        newEdge.setAttribute("toid", String.valueOf(myObject.getId()));
        rootNode.appendChild(newEdge);
    }

    public void addEdgeBetweenSubAndComp(Document input, Component component, Subsystem subsystem, boolean isnative) {
        Node rootNode = input.getDocumentElement();
        Element newEdge = input.createElement("edge");
        newEdge.setAttribute("type", "SUBSYSTEM_COMPONENT");
        newEdge.setAttribute("fromid", String.valueOf(subsystem.getId()));
        newEdge.setAttribute("toid", String.valueOf(component.getId()));

        Element propertyIsnative = input.createElement("property");
        propertyIsnative.setAttribute("name", "isnative");
        propertyIsnative.setAttribute("value", String.valueOf(isnative));

        rootNode.appendChild(newEdge);
        newEdge.appendChild(propertyIsnative);
    }

    public void addEdgeBetweenObjAndInc(Document input, Incident incident, List<ObjectIncident> objectIncidents) {
        Node rootNode = input.getDocumentElement();

        for (ObjectIncident objectIncident : objectIncidents) {
            Element newEdge = input.createElement("edge");
            newEdge.setAttribute("type", "OBJECT_INCIDENT");
            newEdge.setAttribute("fromid", String.valueOf(objectIncident.getFromId()));
            newEdge.setAttribute("toid", String.valueOf(incident.getId()));

            Element propertyIssuedby = input.createElement("property");
            propertyIssuedby.setAttribute("name", "issuedby");
            propertyIssuedby.setAttribute("value", String.valueOf(objectIncident.issuedby()));

            Element propertyAffect = input.createElement("property");
            propertyAffect.setAttribute("name", "affect");
            propertyAffect.setAttribute("value", String.valueOf(objectIncident.isAffect()));

            Element propertyComment = input.createElement("property");
            propertyComment.setAttribute("name", "comment");
            propertyComment.setAttribute("value", String.valueOf(objectIncident.getComment()));

            rootNode.appendChild(newEdge);
            newEdge.appendChild(propertyIssuedby);
            newEdge.appendChild(propertyAffect);
            newEdge.appendChild(propertyComment);
        }
    }

    public void addEdgeBetweenIncAndInc(Document input, Incident incident, List<IncidentIncident> incidentIncidents) {
        Node rootNode = input.getDocumentElement();

        for (IncidentIncident incidentIncident : incidentIncidents) {
            Element newEdge = input.createElement("edge");
            newEdge.setAttribute("type", "INCIDENT_INCIDENT");
            newEdge.setAttribute("fromid", String.valueOf(incidentIncident.getFromId()));
            newEdge.setAttribute("toid", String.valueOf(incident.getId()));

            Element propertyIssuedby = input.createElement("property");
            propertyIssuedby.setAttribute("name", "issuedby");
            propertyIssuedby.setAttribute("value", String.valueOf(incidentIncident.issuedby()));

            Element propertyAffect = input.createElement("property");
            propertyAffect.setAttribute("name", "leadto");
            propertyAffect.setAttribute("value", String.valueOf(incidentIncident.isLeadto()));

            Element propertyComment = input.createElement("property");
            propertyComment.setAttribute("name", "comment");
            propertyComment.setAttribute("value", String.valueOf(incidentIncident.getComment()));

            rootNode.appendChild(newEdge);
            newEdge.appendChild(propertyIssuedby);
            newEdge.appendChild(propertyAffect);
            newEdge.appendChild(propertyComment);
        }
    }

    //-------------------------------------- ADD END -------------------------------------------------------

    //-------------------------------------- GET ALL -------------------------------------------------------
    public List<Enterprise> getAllEnterprises (Document input) {

        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<Enterprise> list = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++){
            Enterprise enterprise = getEnterprise(nodeList.item(i));

            if (enterprise != null) {
                list.add(enterprise);
            }
        }
        return list;
    }

    public List<Subsystem> getAllSubsystems (Document input) {

        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<Subsystem> list = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++){
            Subsystem subsystem = getSubsystem(nodeList.item(i));

            if (subsystem != null) {
                list.add(subsystem);
            }
        }
        return list;
    }

    public List<Component> getAllComponent (Document input) {

        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<Component> list = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++){
            Component component = getComponent(nodeList.item(i));

            if (component != null) {
                list.add(component);
            }
        }
        return list;
    }

    public List<ObjGroup> getAllObjGroups (Document input) {

        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<ObjGroup> list = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++){
            ObjGroup objGroup = getObjGroup(nodeList.item(i));

            if (objGroup != null) {
                list.add(objGroup);
            }
        }
        return list;
    }

    public List<MyObject> getAllObjects (Document input) {

        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<MyObject> list = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++){
            MyObject myObject = getObject(nodeList.item(i));

            if (myObject != null) {
                list.add(myObject);
            }
        }
        return list;
    }

    public List<Incident> getAllIncidents (Document input) {

        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<Incident> list = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++){
            Incident incident = getIncident(nodeList.item(i));

            if (incident != null) {
                list.add(incident);
            }
        }
        return list;
    }

    //-------------------------------------- GET ALL END -------------------------------------------------------
    //-------------------------------------- GET BY -------------------------------------------------------

    public List<Subsystem> getSubsystemsByEnterprise (Document input, Enterprise enterprise) {

        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<String> list = new ArrayList<>();
        List<Subsystem> subsystemList = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++){
            String toId = getSubIdByEnterprise(nodeList.item(i), enterprise);

            if (toId != null) {
                list.add(toId);
            }
        }

        for (int i = 0; i < nodeList.getLength(); i++){
            Subsystem subsystem = getSubsystemByIds(nodeList.item(i), list);

            if (subsystem != null) {
                subsystemList.add(subsystem);
            }
        }

        return subsystemList;
    }

    public List<Enterprise> getEnterprisesBySubsystem (Document input, Subsystem subsystem) {

        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<String> list = new ArrayList<>();
        List<Enterprise> enterpriseList = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++){
            String fromId = getEnterIdBySubsystem(nodeList.item(i), subsystem);

            if (fromId != null) {
                list.add(fromId);
            }
        }

        for (int i = 0; i < nodeList.getLength(); i++){
            Enterprise enterprise = getEnterpriseByIds(nodeList.item(i), list);

            if (enterprise != null) {
                enterpriseList.add(enterprise);
            }
        }

        return enterpriseList;
    }

    public List<Component> getComponentsBySubsystem (Document input, Subsystem subsystem) {

        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<String> list = new ArrayList<>();
        List<Component> componentList = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            String fromId = getCompIdBySubsystem(nodeList.item(i), subsystem);

            if (fromId != null) {
                list.add(fromId);
            }
        }

        for (int i = 0; i < nodeList.getLength(); i++) {
            Component component = getComponentByIds(nodeList.item(i), list);

            if (component != null) {
                componentList.add(component);
            }
        }

        return componentList;
    }

    public List<Subsystem> getSubsystemByComponent (Document input, Component component) {
        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<String> list = new ArrayList<>();
        List<Subsystem> subsystemList = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++){
            String fromId = getSubIdByComp(nodeList.item(i), component);

            if (fromId != null) {
                list.add(fromId);
            }
        }

        for (int i = 0; i < nodeList.getLength(); i++){
            Subsystem subsystem = getSubsystemByIds(nodeList.item(i), list);

            if (subsystem != null) {
                subsystemList.add(subsystem);
            }
        }

        return subsystemList;
    }

    public List<Component> getComponentsByObjGroup (Document input, ObjGroup objGroup) {
        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<String> list = new ArrayList<>();
        List<Component> componentList = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++){
            String fromId = getCompIdByObjGrp(nodeList.item(i), objGroup);

            if (fromId != null) {
                list.add(fromId);
            }
        }

        for (int i = 0; i < nodeList.getLength(); i++){
            Component component = getComponentByIds(nodeList.item(i), list);

            if (component != null) {
                componentList.add(component);
            }
        }

        return componentList;
    }


    public List<ObjGroup> getObjGroupsByObject (Document input, MyObject myObject) {
        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<String> list = new ArrayList<>();
        List<ObjGroup> objGroupList = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++){
            String fromId = getObjGrpIdByObj(nodeList.item(i), myObject);

            if (fromId != null) {
                list.add(fromId);
            }
        }

        for (int i = 0; i < nodeList.getLength(); i++){
            ObjGroup objGroup = getObjGroupsByIds(nodeList.item(i), list);

            if (objGroup != null) {
                objGroupList.add(objGroup);
            }
        }

        return objGroupList;
    }

    public List<MyObject> getObjectsByIncident (Document input, Incident incident) {
        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<String> list = new ArrayList<>();
        List<MyObject> myObjectList = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++){
            String fromId = getObjIdByIncident(nodeList.item(i), incident);

            if (fromId != null) {
                list.add(fromId);
            }
        }

        for (int i = 0; i < nodeList.getLength(); i++){
            MyObject myObject = getObjectByIds(nodeList.item(i), list);

            if (myObject != null) {
                myObjectList.add(myObject);
            }
        }

        return myObjectList;
    }

    public List<ObjGroup> getsObjGroupByComponent (Document input, Component component) {

        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<String> list = new ArrayList<>();
        List<ObjGroup> objGroups = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            String fromId = getObjGrpIdBySubsystem(nodeList.item(i), component);

            if (fromId != null) {
                list.add(fromId);
            }
        }

        for (int i = 0; i < nodeList.getLength(); i++) {
            ObjGroup objGroup = getObjGroupsByIds(nodeList.item(i), list);

            if (objGroup != null) {
                objGroups.add(objGroup);
            }
        }

        return objGroups;
    }

    public List<MyObject> getObjectsByObjGroup (Document input, ObjGroup objGroup) {

        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<String> list = new ArrayList<>();
        List<MyObject> myObjectList = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            String fromId = getObjectIdByObjGrp(nodeList.item(i), objGroup);

            if (fromId != null) {
                list.add(fromId);
            }
        }

        for (int i = 0; i < nodeList.getLength(); i++) {
            MyObject myObject = getObjectByIds(nodeList.item(i), list);

            if (myObject != null) {
                myObjectList.add(myObject);
            }
        }

        return myObjectList;
    }

    public List<Incident> getIncidentsByObject (Document input, MyObject object) {

        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<String> list = new ArrayList<>();
        List<Incident> incidentList = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            String fromId = getIncidentIdByObject(nodeList.item(i), object);

            if (fromId != null) {
                list.add(fromId);
            }
        }

        for (int i = 0; i < nodeList.getLength(); i++) {
            Incident incident = getIncidentByIds(nodeList.item(i), list);

            if (incident != null) {
                incidentList.add(incident);
            }
        }

        return incidentList;
    }

    public List<Incident> getIncidentsByObject (Document input, IncidentLink incidentLink) {

        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<String> list = new ArrayList<>();
        List<Incident> incidentList = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            String fromId = getIncidentIdByObject(nodeList.item(i), incidentLink);

            if (fromId != null) {
                list.add(fromId);
            }
        }

        for (int i = 0; i < nodeList.getLength(); i++) {
            Incident incident = getIncidentByIds(nodeList.item(i), list);

            if (incident != null) {
                incidentList.add(incident);
            }
        }

        return incidentList;
    }

    public List<Incident> getIncidentsByIncident (Document input, Incident incident) {

        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<String> list = new ArrayList<>();
        List<Incident> incidentList = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            String fromId = getIncidentIdByIncident(nodeList.item(i), incident);

            if (fromId != null) {
                list.add(fromId);
            }
        }

        for (int i = 0; i < nodeList.getLength(); i++) {
            Incident inc = getIncidentByIds(nodeList.item(i), list);

            if (inc != null) {
                incidentList.add(inc);
            }
        }

        return incidentList;
    }

    //-------------------------------------- GET BY END -------------------------------------------------------

//    private String parseNode(Node node) {
//        if (node.getNodeName().equals("#text")) {
//            return "";
//        }
//
//        StringBuilder result = new StringBuilder();
//        result.append("Element name = '" + node.getNodeName() + "'\n");
//        NamedNodeMap nodeMap = node.getAttributes();
//
//        if(nodeMap != null) {
//            for (int i = 0; i < nodeMap.getLength(); i++) {
//                result.append("Attribute name = '" + nodeMap.item(i).getNodeName()
//                        + "'; Attribute value = '" + nodeMap.item(i).getNodeValue() + "'\n");
//            }
//        }
//        if (getElementContent(node) != null && !(getElementContent(node).equals("")))
//            result.append("Element content = '").append(getElementContent(node)).append("'\n");
//
//        NodeList nodeList = node.getChildNodes();
//
//        for (int i = 0; i < nodeList.getLength(); i++){
//            result.append(parseNode(nodeList.item(i)));
//        }
//
//        result.append("Element closed, name = '" + node.getNodeName() + "'\n");
//
//        return result.toString();
//
//    }
//
//    private String getElementContent(Node node) {
//
//        Node contentNode = node.getFirstChild();
//        if (contentNode != null)
//
//            if (contentNode.getNodeName().equals("#text")) {
//                String value = contentNode.getNodeValue();
//                if (value != null)
//                    return value.trim();
//            }
//        return null;
//    }

    //-------------------------------------- GET OBJ -------------------------------------------------------
    private Enterprise getEnterprise(Node node) {
        String name = node.getNodeName();

        Enterprise enterprise = null;
        if (name.equals("node")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String id = nodeMap.getNamedItem("id").getNodeValue();
                if (type.equals("ENTERPRISE")) {
                    enterprise = new Enterprise(Integer.parseInt(id));
                    NodeList nodeList = node.getChildNodes();

                    for (int i = 0; i < nodeList.getLength(); i++){
                        enterpriseProperty(nodeList.item(i), enterprise);
                    }
                }
            }
        }

        return enterprise;
    }

    private Subsystem getSubsystem(Node node) {
        String name = node.getNodeName();

        Subsystem subsystem = null;
        if (name.equals("node")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String id = nodeMap.getNamedItem("id").getNodeValue();
                if (type.equals("SUBSYSTEM")) {
                    subsystem = new Subsystem(Integer.parseInt(id));
                    NodeList nodeList = node.getChildNodes();

                    for (int i = 0; i < nodeList.getLength(); i++){
                        subsystemProperty(nodeList.item(i), subsystem);
                    }
                }
            }
        }

        return subsystem;
    }

    private Component getComponent(Node node) {
        String name = node.getNodeName();

        Component component = null;
        if (name.equals("node")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String id = nodeMap.getNamedItem("id").getNodeValue();
                if (type.equals("COMPONENT")) {
                    component = new Component(Integer.parseInt(id));
                    NodeList nodeList = node.getChildNodes();

                    for (int i = 0; i < nodeList.getLength(); i++){
                        componentProperty(nodeList.item(i), component);
                    }
                }
            }
        }

        return component;
    }

    private ObjGroup getObjGroup(Node node) {
        String name = node.getNodeName();

        ObjGroup objGroup = null;
        if (name.equals("node")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String id = nodeMap.getNamedItem("id").getNodeValue();
                if (type.equals("OBJGROUP")) {
                    objGroup = new ObjGroup(Integer.parseInt(id));
                    NodeList nodeList = node.getChildNodes();

                    for (int i = 0; i < nodeList.getLength(); i++){
                        objGroupProperty(nodeList.item(i), objGroup);
                    }
                }
            }
        }

        return objGroup;
    }

    private MyObject getObject(Node node) {
        String name = node.getNodeName();

        MyObject myObject = null;
        if (name.equals("node")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String id = nodeMap.getNamedItem("id").getNodeValue();
                if (type.equals("OBJECT")) {
                    myObject = new MyObject(Integer.parseInt(id));
                    NodeList nodeList = node.getChildNodes();

                    for (int i = 0; i < nodeList.getLength(); i++){
                        objectProperty(nodeList.item(i), myObject);
                    }
                }
            }
        }

        return myObject;
    }

    private Incident getIncident(Node node) {
        String name = node.getNodeName();

        Incident incident = null;
        if (name.equals("node")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String id = nodeMap.getNamedItem("id").getNodeValue();
                if (type.equals("INCIDENT")) {
                    incident = new Incident(Integer.parseInt(id));
                    NodeList nodeList = node.getChildNodes();

                    for (int i = 0; i < nodeList.getLength(); i++){
                        incidentProperty(nodeList.item(i), incident);
                    }
                }
            }
        }

        return incident;
    }

    //-------------------------------------- GET OBJ END -------------------------------------------------------
    //-------------------------------------- GET NODE -------------------------------------------------------
    private Node getEnterpriseNodeById(Document input, String searchedId) {
        NodeList nodeList = input.getElementsByTagName("node");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String type = nodeMap.getNamedItem("type").getNodeValue();
            String id = nodeMap.getNamedItem("id").getNodeValue();
            if (type.equals("ENTERPRISE") && searchedId.equals(id)) {
                return item;
            }
        }
        return null;
    }

    private Node getSubsystemNodeById(Document input, String searchedId) {
        NodeList nodeList = input.getElementsByTagName("node");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String type = nodeMap.getNamedItem("type").getNodeValue();
            String id = nodeMap.getNamedItem("id").getNodeValue();
            if (type.equals("SUBSYSTEM") && searchedId.equals(id)) {
                return item;
            }
        }
        return null;
    }

    private Node getComponentNodeById(Document input, String searchedId) {
        NodeList nodeList = input.getElementsByTagName("node");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String type = nodeMap.getNamedItem("type").getNodeValue();
            String id = nodeMap.getNamedItem("id").getNodeValue();
            if (type.equals("COMPONENT") && searchedId.equals(id)) {
                return item;
            }
        }
        return null;
    }

    private Node getObjGroupNodeById(Document input, String searchedId) {
        NodeList nodeList = input.getElementsByTagName("node");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String type = nodeMap.getNamedItem("type").getNodeValue();
            String id = nodeMap.getNamedItem("id").getNodeValue();
            if (type.equals("OBJGROUP") && searchedId.equals(id)) {
                return item;
            }
        }
        return null;
    }

    private Node getObjectNodeById(Document input, String searchedId) {
        NodeList nodeList = input.getElementsByTagName("node");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String type = nodeMap.getNamedItem("type").getNodeValue();
            String id = nodeMap.getNamedItem("id").getNodeValue();
            if (type.equals("OBJECT") && searchedId.equals(id)) {
                return item;
            }
        }
        return null;
    }

    private Node getIncidentNodeById(Document input, String searchedId) {
        NodeList nodeList = input.getElementsByTagName("node");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String type = nodeMap.getNamedItem("type").getNodeValue();
            String id = nodeMap.getNamedItem("id").getNodeValue();
            if (type.equals("INCIDENT") && searchedId.equals(id)) {
                return item;
            }
        }
        return null;
    }

    private List<Node> getEdgeBetweenEnterSub (Document input, Enterprise enterprise) {
        NodeList nodeList = input.getElementsByTagName("edge");
        String enterId = String.valueOf(enterprise.getId());
        List<Node> listNodes = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String fromId = nodeMap.getNamedItem("fromid").getNodeValue();
            if (fromId.equals(enterId)) {
                listNodes.add(item);
            }
        }

        return listNodes;
    }

    private List<Node> getEdgeSubsystem (Document input, Subsystem subsystem) {
        NodeList nodeList = input.getElementsByTagName("edge");
        String currId = String.valueOf(subsystem.getId());
        List<Node> listNodes = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String fromId = nodeMap.getNamedItem("fromid").getNodeValue();
            String toId = nodeMap.getNamedItem("toid").getNodeValue();
            if (fromId.equals(currId) || toId.equals(currId)) {
                listNodes.add(item);
            }
        }

        return listNodes;
    }


    private List<Node> getEdgeSubsystemSingle (Document input, Subsystem subsystem) {
        NodeList nodeList = input.getElementsByTagName("edge");
        String currId = String.valueOf(subsystem.getId());
        List<Node> listNodes = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String toId = nodeMap.getNamedItem("toid").getNodeValue();
            if (toId.equals(currId)) {
                listNodes.add(item);
            }
        }

        return listNodes;
    }

    private List<Node> getEdgeComponent (Document input, Component component) {
        NodeList nodeList = input.getElementsByTagName("edge");
        String currId = String.valueOf(component.getId());
        List<Node> listNodes = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String fromId = nodeMap.getNamedItem("fromid").getNodeValue();
            String toId = nodeMap.getNamedItem("toid").getNodeValue();
            if (fromId.equals(currId) || toId.equals(currId)) {
                listNodes.add(item);
            }
        }

        return listNodes;
    }

    private List<Node> getEdgeComponentSingle (Document input, Component component) {
        NodeList nodeList = input.getElementsByTagName("edge");
        String currId = String.valueOf(component.getId());
        List<Node> listNodes = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
//            String fromId = nodeMap.getNamedItem("fromid").getNodeValue();
            String toId = nodeMap.getNamedItem("toid").getNodeValue();
            if (toId.equals(currId)) {
                listNodes.add(item);
            }
        }

        return listNodes;
    }


    private List<Node> getEdgeObjectGroup (Document input, ObjGroup objGroup) {
        NodeList nodeList = input.getElementsByTagName("edge");
        String currId = String.valueOf(objGroup.getId());
        List<Node> listNodes = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String fromId = nodeMap.getNamedItem("fromid").getNodeValue();
            String toId = nodeMap.getNamedItem("toid").getNodeValue();
            if (fromId.equals(currId) || toId.equals(currId)) {
                listNodes.add(item);
            }
        }

        return listNodes;
    }

    private List<Node> getEdgeObjectGroupSingle (Document input, ObjGroup objGroup) {
        NodeList nodeList = input.getElementsByTagName("edge");
        String currId = String.valueOf(objGroup.getId());
        List<Node> listNodes = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
//            String fromId = nodeMap.getNamedItem("fromid").getNodeValue();
            String toId = nodeMap.getNamedItem("toid").getNodeValue();
            if (toId.equals(currId)) {
                listNodes.add(item);
            }
        }

        return listNodes;
    }

    private List<Node> getEdgeObject (Document input, MyObject myObject) {
        NodeList nodeList = input.getElementsByTagName("edge");
        String currId = String.valueOf(myObject.getId());
        List<Node> listNodes = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String fromId = nodeMap.getNamedItem("fromid").getNodeValue();
            String toId = nodeMap.getNamedItem("toid").getNodeValue();
            if (fromId.equals(currId) || toId.equals(currId)) {
                listNodes.add(item);
            }
        }

        return listNodes;
    }

    private List<Node> getEdgeObjectSingle (Document input, MyObject myObject) {
        NodeList nodeList = input.getElementsByTagName("edge");
        String currId = String.valueOf(myObject.getId());
        List<Node> listNodes = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String toId = nodeMap.getNamedItem("toid").getNodeValue();
            if (toId.equals(currId)) {
                listNodes.add(item);
            }
        }

        return listNodes;
    }

    private List<Node> getEdgeIncident (Document input, Incident incident) {
        NodeList nodeList = input.getElementsByTagName("edge");
        String currId = String.valueOf(incident.getId());
        List<Node> listNodes = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String fromId = nodeMap.getNamedItem("fromid").getNodeValue();
            String toId = nodeMap.getNamedItem("toid").getNodeValue();
            if (fromId.equals(currId) || toId.equals(currId)) {
                listNodes.add(item);
            }
        }

        return listNodes;
    }

    private List<Node> getEdgeIncidentIncident (Document input, Incident incident) {
        NodeList nodeList = input.getElementsByTagName("edge");
        String currId = String.valueOf(incident.getId());
        List<Node> listNodes = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String type = nodeMap.getNamedItem("type").getNodeValue();
            String fromId = nodeMap.getNamedItem("fromid").getNodeValue();
            String toId = nodeMap.getNamedItem("toid").getNodeValue();
            if ((fromId.equals(currId) || toId.equals(currId)) && type.equals("INCIDENT_INCIDENT")) {
                listNodes.add(item);
            }
        }

        return listNodes;
    }

    private List<Node> getEdgeObjectIncident (Document input, Incident incident) {
        NodeList nodeList = input.getElementsByTagName("edge");
        String currId = String.valueOf(incident.getId());
        List<Node> listNodes = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String type = nodeMap.getNamedItem("type").getNodeValue();
            String toId = nodeMap.getNamedItem("toid").getNodeValue();
            if (toId.equals(currId) && type.equals("OBJECT_INCIDENT")) {
                listNodes.add(item);
            }
        }

        return listNodes;
    }

    //-------------------------------------- GET NODE END -------------------------------------------------------

    //-------------------------------------- GET PROPERTIES -------------------------------------------------------
    private void enterpriseProperty(Node node, Enterprise enterprise) {
        String nameNod = node.getNodeName();

        if (nameNod.equals("property")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String name = nodeMap.getNamedItem("name").getNodeValue();
                String value = nodeMap.getNamedItem("value").getNodeValue();
                if (name.equals("enterprisename")) {
                    enterprise.setEnterpriseName(value);
                }

                if (name.equals("comment")) {
                    enterprise.setComment(value);
                }
            }
        }
    }

    private void subsystemProperty(Node node, Subsystem subsystem) {
        String nameNod = node.getNodeName();

        if (nameNod.equals("property")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String name = nodeMap.getNamedItem("name").getNodeValue();
                String value = nodeMap.getNamedItem("value").getNodeValue();
                if (name.equals("subname")) {
                    subsystem.setSubsystemName(value);
                }

                if (name.equals("comment")) {
                    subsystem.setComment(value);
                }
            }
        }
    }

    private void componentProperty(Node node, Component component) {
        String nameNod = node.getNodeName();

        if (nameNod.equals("property")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String name = nodeMap.getNamedItem("name").getNodeValue();
                String value = nodeMap.getNamedItem("value").getNodeValue();
                if (name.equals("compname")) {
                    component.setCompName(value);
                }

                if (name.equals("comment")) {
                    component.setComment(value);
                }

                if (name.equals("vendor")) {
                    component.setVendor(value);
                }
            }
        }
    }

    private void objGroupProperty(Node node, ObjGroup objGroup) {
        String nameNod = node.getNodeName();

        if (nameNod.equals("property")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String name = nodeMap.getNamedItem("name").getNodeValue();
                String value = nodeMap.getNamedItem("value").getNodeValue();
                if (name.equals("objgname")) {
                    objGroup.setObjgname(value);
                }

                if (name.equals("belongsto")) {
                    objGroup.setBelongsto(value);
                }
            }
        }
    }

    private void objectProperty(Node node, MyObject myObject) {
        String nameNod = node.getNodeName();

        if (nameNod.equals("property")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String name = nodeMap.getNamedItem("name").getNodeValue();
                String value = nodeMap.getNamedItem("value").getNodeValue();
                if (name.equals("objname")) {
                    myObject.setObjname(value);
                }

                if (name.equals("belongsto")) {
                    myObject.setBelongsto(value);
                }
            }
        }
    }

    private void incidentProperty(Node node, Incident incident) {
        String nameNod = node.getNodeName();

        if (nameNod.equals("property")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String name = nodeMap.getNamedItem("name").getNodeValue();
                String value = nodeMap.getNamedItem("value").getNodeValue();
                if (name.equals("code")) {
                    incident.setCode(value);
                }

                if (name.equals("message")) {
                    incident.setMessage(value);
                }

                if (name.equals("severity")) {
                    incident.setSeverity(value);
                }

                if (name.equals("type")) {
                    incident.setType(value);
                }

                if (name.equals("event")) {
                    incident.setEvent(value);
                }

                if (name.equals("action")) {
                    incident.setAction(value);
                }

                if (name.equals("osi")) {
                    incident.setOsi(Integer.parseInt(value));
                }

                if (name.equals("comment")) {
                    incident.setComment(value);
                }
            }
        }
    }


//-------------------------------------- GET PROPERTIES END -------------------------------------------------------
//-------------------------------------- GET BY IDS -------------------------------------------------------


    private Subsystem getSubsystemByIds(Node node, List<String> listIds) {
        String name = node.getNodeName();

        Subsystem subsystem = null;
        if (name.equals("node")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String id = nodeMap.getNamedItem("id").getNodeValue();
                if (type.equals("SUBSYSTEM")) {
                    for (String strId : listIds) {
                        if (strId.equals(id)){
                            subsystem = new Subsystem(Integer.parseInt(id));
                            NodeList nodeList = node.getChildNodes();

                            for (int i = 0; i < nodeList.getLength(); i++){
                                subsystemProperty(nodeList.item(i), subsystem);
                            }
                            break;
                        }
                    }

                }
            }
        }

        return subsystem;
    }

    private Enterprise getEnterpriseByIds(Node node, List<String> listIds) {
        String name = node.getNodeName();

        Enterprise enterprise = null;
        if (name.equals("node")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String id = nodeMap.getNamedItem("id").getNodeValue();
                if (type.equals("ENTERPRISE")) {
                    for (String strId : listIds) {
                        if (strId.equals(id)){
                            enterprise = new Enterprise(Integer.parseInt(id));
                            NodeList nodeList = node.getChildNodes();

                            for (int i = 0; i < nodeList.getLength(); i++){
                                enterpriseProperty(nodeList.item(i), enterprise);
                            }
                            break;
                        }
                    }

                }
            }
        }

        return enterprise;
    }

    private Component getComponentByIds(Node node, List<String> listIds) {
        String name = node.getNodeName();

        Component component = null;
        if (name.equals("node")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String id = nodeMap.getNamedItem("id").getNodeValue();
                if (type.equals("COMPONENT")) {
                    for (String strId : listIds) {
                        if (strId.equals(id)){
                            component = new Component(Integer.parseInt(id));
                            NodeList nodeList = node.getChildNodes();

                            for (int i = 0; i < nodeList.getLength(); i++){
                                componentProperty(nodeList.item(i), component);
                            }
                            break;
                        }
                    }

                }
            }
        }

        return component;
    }

    private ObjGroup getObjGroupsByIds(Node node, List<String> listIds) {
        String name = node.getNodeName();

        ObjGroup objGroup = null;
        if (name.equals("node")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String id = nodeMap.getNamedItem("id").getNodeValue();
                if (type.equals("OBJGROUP")) {
                    for (String strId : listIds) {
                        if (strId.equals(id)){
                            objGroup = new ObjGroup(Integer.parseInt(id));
                            NodeList nodeList = node.getChildNodes();

                            for (int i = 0; i < nodeList.getLength(); i++){
                                objGroupProperty(nodeList.item(i), objGroup);
                            }
                            break;
                        }
                    }

                }
            }
        }

        return objGroup;
    }

    private MyObject getObjectByIds(Node node, List<String> listIds) {
        String name = node.getNodeName();

        MyObject myObject = null;
        if (name.equals("node")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String id = nodeMap.getNamedItem("id").getNodeValue();
                if (type.equals("OBJECT")) {
                    for (String strId : listIds) {
                        if (strId.equals(id)){
                            myObject = new MyObject(Integer.parseInt(id));
                            NodeList nodeList = node.getChildNodes();

                            for (int i = 0; i < nodeList.getLength(); i++){
                                objectProperty(nodeList.item(i), myObject);
                            }
                            break;
                        }
                    }

                }
            }
        }

        return myObject;
    }

    private Incident getIncidentByIds(Node node, List<String> listIds) {
        String name = node.getNodeName();

        Incident incident = null;
        if (name.equals("node")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String id = nodeMap.getNamedItem("id").getNodeValue();
                if (type.equals("INCIDENT")) {
                    for (String strId : listIds) {
                        if (strId.equals(id)){
                            incident = new Incident(Integer.parseInt(id));
                            NodeList nodeList = node.getChildNodes();

                            for (int i = 0; i < nodeList.getLength(); i++){
                                incidentProperty(nodeList.item(i), incident);
                            }
                            break;
                        }
                    }

                }
            }
        }

        return incident;
    }

    //-------------------------------------- GET BY IDS END -------------------------------------------------------
    //---------------------------getting edges------------------------------------------------------

    private String getSubIdByEnterprise(Node node, Enterprise enterprise) {
        String name = node.getNodeName();
        String enterId = String.valueOf(enterprise.getId());

        if (name.equals("edge")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String toid = nodeMap.getNamedItem("toid").getNodeValue();
                String fromid = nodeMap.getNamedItem("fromid").getNodeValue();
                if (type.equals("ENTERPRISE_SUBSYSTEM") && fromid.equals(enterId)) {
                    return toid;
                }
            }
        }

        return null;
    }

    private String getEnterIdBySubsystem(Node node, Subsystem subsystem) {
        String name = node.getNodeName();
        String subId = String.valueOf(subsystem.getId());

        if (name.equals("edge")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String toid = nodeMap.getNamedItem("toid").getNodeValue();
                String fromid = nodeMap.getNamedItem("fromid").getNodeValue();
                if (type.equals("ENTERPRISE_SUBSYSTEM") && toid.equals(subId)) {
                    return fromid;
                }
            }
        }

        return null;
    }

    private String getSubIdByComp(Node node, Component component) {
        String name = node.getNodeName();
        String subId = String.valueOf(component.getId());

        if (name.equals("edge")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String toid = nodeMap.getNamedItem("toid").getNodeValue();
                if (type.equals("SUBSYSTEM_COMPONENT") && toid.equals(subId)) {
                    return nodeMap.getNamedItem("fromid").getNodeValue();
                }
            }
        }

        return null;
    }

    private String getCompIdByObjGrp (Node node, ObjGroup objGroup) {
        String name = node.getNodeName();
        String subId = String.valueOf(objGroup.getId());

        if (name.equals("edge")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String toid = nodeMap.getNamedItem("toid").getNodeValue();
                if (type.equals("COMPONENT_OBJGROUP") && toid.equals(subId)) {
                    return nodeMap.getNamedItem("fromid").getNodeValue();
                }
            }
        }

        return null;
    }

    private String getObjGrpIdByObj (Node node, MyObject myObject) {
        String name = node.getNodeName();
        String subId = String.valueOf(myObject.getId());

        if (name.equals("edge")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String toid = nodeMap.getNamedItem("toid").getNodeValue();
                if (type.equals("OBJGROUP_OBJECT") && toid.equals(subId)) {
                    return nodeMap.getNamedItem("fromid").getNodeValue();
                }
            }
        }

        return null;
    }

    private String getObjIdByIncident (Node node, Incident incident) {
        String name = node.getNodeName();
        String subId = String.valueOf(incident.getId());

        if (name.equals("edge")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String toid = nodeMap.getNamedItem("toid").getNodeValue();
                if (type.equals("OBJECT_INCIDENT") && toid.equals(subId)) {
                    return nodeMap.getNamedItem("fromid").getNodeValue();
                }
            }
        }

        return null;
    }


    private String getCompIdBySubsystem(Node node, Subsystem subsystem) {
        String name = node.getNodeName();
        String subId = String.valueOf(subsystem.getId());

        if (name.equals("edge")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String toid = nodeMap.getNamedItem("toid").getNodeValue();
                String fromid = nodeMap.getNamedItem("fromid").getNodeValue();
                if (type.equals("SUBSYSTEM_COMPONENT") && fromid.equals(subId)) {
                    return toid;
                }
            }
        }

        return null;
    }

    private String getObjGrpIdBySubsystem(Node node, Component component) {
        String name = node.getNodeName();
        String subId = String.valueOf(component.getId());

        if (name.equals("edge")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String toid = nodeMap.getNamedItem("toid").getNodeValue();
                String fromid = nodeMap.getNamedItem("fromid").getNodeValue();
                if (type.equals("COMPONENT_OBJGROUP") && fromid.equals(subId)) {
                    return toid;
                }
            }
        }

        return null;
    }

    private String getObjectIdByObjGrp (Node node, ObjGroup objGroup) {
        String name = node.getNodeName();
        String subId = String.valueOf(objGroup.getId());

        if (name.equals("edge")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String toid = nodeMap.getNamedItem("toid").getNodeValue();
                String fromid = nodeMap.getNamedItem("fromid").getNodeValue();
                if (type.equals("OBJGROUP_OBJECT") && fromid.equals(subId)) {
                    return toid;
                }
            }
        }

        return null;
    }

    private String getIncidentIdByObject (Node node, MyObject object) {
        String name = node.getNodeName();
        String subId = String.valueOf(object.getId());

        if (name.equals("edge")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String toid = nodeMap.getNamedItem("toid").getNodeValue();
                String fromid = nodeMap.getNamedItem("fromid").getNodeValue();
                if (type.equals("OBJECT_INCIDENT") && fromid.equals(subId)) {
                    return toid;
                }
            }
        }

        return null;
    }

    private String getIncidentIdByObject (Node node, IncidentLink incidentLink) {
        String name = node.getNodeName();
        String subId = String.valueOf(incidentLink.getFromId());

        if (name.equals("edge")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String toid = nodeMap.getNamedItem("toid").getNodeValue();
                String fromid = nodeMap.getNamedItem("fromid").getNodeValue();
                if ((type.equals("OBJECT_INCIDENT") || type.equals("INCIDENT_INCIDENT")) &&(fromid.equals(subId) || toid.equals(subId))) {//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    return toid;
                }
            }
        }

        return null;
    }

    private String getIncidentIdByIncident (Node node, Incident incident) {
        String name = node.getNodeName();
        String subId = String.valueOf(incident.getId());

        if (name.equals("edge")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String toid = nodeMap.getNamedItem("toid").getNodeValue();
                String fromid = nodeMap.getNamedItem("fromid").getNodeValue();
                if (type.equals("INCIDENT_INCIDENT")) {
                    if (fromid.equals(subId)) {
                        return toid;
                    }

                    if (toid.equals(subId)) {
                        return fromid;
                    }
                }
            }
        }

        return null;
    }

//---------------------------getting edges end------------------------------------------------------
//-------------------------------------- GET IDS ---------------------------------------------------

    protected int getIdObjGroup (Document input, ObjGroup objGroup, Component component){
        List<ObjGroup> listObjGrps = getsObjGroupByComponent(input, component);

        int numEnterprises = listObjGrps.size();//!!!!!!!!!!!!!!!!!!!!!!!!!
        if (numEnterprises < 9){
            numEnterprises++;
            objGroup.setId(numEnterprises + component.getId() * 10);
            return numEnterprises + component.getId() * 10;
        }

        listObjGrps = getAllObjGroups(input);
        listObjGrps.sort((o1, o2) -> {
            if (o1.getId() > o2.getId()) return 1;
            if (o1.getId() < o2.getId()) return -1;
            return 0;
        });

        int prevId = listObjGrps.get(0).getId();
        for (ObjGroup s : listObjGrps) {
            if (prevId + 1 != s.getId()) {
                objGroup.setId(prevId + 1);
                return prevId + 1;
            }
        }

        return -1;
    }

    protected int getIdSubSystem (Document input, Subsystem subsystem, Enterprise enterprise){
        List<Subsystem> listSub = getSubsystemsByEnterprise(input, enterprise);

        int numEnterprises = listSub.size();//!!!!!!!!!!!!!!!!!!!!!!!!!
        if (numEnterprises < 9){
            numEnterprises++;
            subsystem.setId(numEnterprises + enterprise.getId() * 10);
            return numEnterprises + enterprise.getId() * 10;
        }

        listSub = getAllSubsystems(input);
        listSub.sort((o1, o2) -> {
            if (o1.getId() > o2.getId()) return 1;
            if (o1.getId() < o2.getId()) return -1;
            return 0;
        });

        int prevId = listSub.get(0).getId();
        for (Subsystem s : listSub) {
            if (prevId + 1 != s.getId()) {
                subsystem.setId(prevId + 1);
                return prevId + 1;
            }
        }

        return -1;
    }

    protected int getIdComponent (Document input, Component component, Subsystem subsystem){
        List<Component> listComponent = getComponentsBySubsystem(input, subsystem);

        int numEnterprises = listComponent.size();//!!!!!!!!!!!!!!!!!!!!!!!!!
        if (numEnterprises < 9){
            numEnterprises++;
            component.setId(numEnterprises + subsystem.getId() * 10);
            return numEnterprises + subsystem.getId() * 10;
        }

        listComponent = getAllComponent(input);
        listComponent.sort((o1, o2) -> {
            if (o1.getId() > o2.getId()) return 1;
            if (o1.getId() < o2.getId()) return -1;
            return 0;
        });

        int prevId = listComponent.get(0).getId();
        for (Component s : listComponent) {
            if (prevId + 1 != s.getId()) {
                component.setId(prevId + 1);
                return prevId + 1;
            }
        }

        return -1;
    }

    protected int getIdObject (Document input, MyObject myObject, ObjGroup objGroup){
        List<MyObject> listSub = getObjectsByObjGroup(input, objGroup);

        int numEnterprises = listSub.size();//!!!!!!!!!!!!!!!!!!!!!!!!!
        if (numEnterprises < 9){
            numEnterprises++;
            myObject.setId(numEnterprises + objGroup.getId() * 10);
            return numEnterprises + objGroup.getId() * 10;
        }

        listSub = getAllObjects(input);
        listSub.sort((o1, o2) -> {
            if (o1.getId() > o2.getId()) return 1;
            if (o1.getId() < o2.getId()) return -1;
            return 0;
        });

        int prevId = listSub.get(0).getId();
        for (MyObject s : listSub) {
            if (prevId + 1 != s.getId()) {
                myObject.setId(prevId + 1);
                return prevId + 1;
            }
        }

        return -1;
    }

    protected int getIdIncident (Document input, Incident incident, IncidentLink incidentLink){
        List<Incident> listSub = getIncidentsByObject(input, incidentLink);

        if (incidentLink != null) {
            int numEnterprises = listSub.size();
            if (numEnterprises < 9){
                numEnterprises++;
                incident.setId(numEnterprises + incidentLink.getFromId() * 10);
                return numEnterprises + incidentLink.getFromId() * 10;
            }
        }

        listSub = getAllIncidents(input);
        listSub.sort((o1, o2) -> {
            if (o1.getId() > o2.getId()) return 1;
            if (o1.getId() < o2.getId()) return -1;
            return 0;
        });

        int prevId = listSub.get(0).getId();
        for (Incident s : listSub) {
            if (prevId + 1 != s.getId()) {
                incident.setId(prevId + 1);
                return prevId + 1;
            }
        }

        return -1;
    }
    //-------------------------------------- GET IDS END -------------------------------------------------------
}
