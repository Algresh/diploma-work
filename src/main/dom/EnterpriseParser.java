package main.dom;

import main.dao.Enterprise;
import main.dao.Subsystem;
import org.w3c.dom.*;

import java.util.ArrayList;
import java.util.List;

public class EnterpriseParser {
    public boolean deleteEnterprise (Document input, Enterprise enterprise) {
        Node node = getEnterpriseNodeById(input, String.valueOf(enterprise.getId()));
        if (node == null) {
            return false;
        }

        NodeList propertyChildrenNotes = node.getChildNodes();

        for (int i = 0; i < propertyChildrenNotes.getLength(); i++) {
            node.removeChild(propertyChildrenNotes.item(i));
        }

        Node parentNode = node.getParentNode();
        parentNode.removeChild(node);

        List<Node> edgesBetweenEnerSub = getEdgeBetweenEnterSub(input, enterprise);
        for (Node item : edgesBetweenEnerSub) {
            parentNode.removeChild(item);
        }

        return true;

    }

    public boolean changeEnterprise (Document input, Enterprise enterprise, List<Subsystem> subOfEnter) {

        Node node = getEnterpriseNodeById(input, String.valueOf(enterprise.getId()));
        if (node == null) {
            return false;
        }

        NodeList listChildren = node.getChildNodes();

        for (int i = 0; i < listChildren.getLength(); i++) {
            Node child = listChildren.item(i);
            if (child.getNodeName().equals("property")) {
                NamedNodeMap childAttr = child.getAttributes();
                String name = childAttr.getNamedItem("name").getNodeValue();

                if (name.equals("enterprisename")) {
                    childAttr.getNamedItem("value").setTextContent(enterprise.getEnterpriseName());
                }
                if (name.equals("comment")) {
                    childAttr.getNamedItem("value").setTextContent(enterprise.getComment());
                }
            }
        }

        Node rootNode = input.getDocumentElement();
        List<Node> edgesBetweenEnerSub = getEdgeBetweenEnterSub(input, enterprise);

        for (Node oldNode : edgesBetweenEnerSub) {
            rootNode.removeChild(oldNode);
        }

        if (subOfEnter != null) {
            for (Subsystem item : subOfEnter) {
                addEdgeBetweenEnterAndSub(input, enterprise, item);
            }
        }
        return true;
    }


    public void addEdgeBetweenEnterAndSub(Document input, Enterprise enterprise, Subsystem subsystem) {
        Node rootNode = input.getDocumentElement();
        Element newEdge = input.createElement("edge");
        newEdge.setAttribute("type", "ENTERPRISE_SUBSYSTEM");
        newEdge.setAttribute("fromid", String.valueOf(enterprise.getId()));
        newEdge.setAttribute("toid", String.valueOf(subsystem.getId()));
        rootNode.appendChild(newEdge);
    }

    public boolean addEnterprise (Document input, Enterprise enterprise) {

        Node rootNode = input.getDocumentElement();

        List<Enterprise> list = getAllEnterprises(input);
        int numEnterprises = list.size();
        if (numEnterprises == 9) return false;
        numEnterprises++;
        enterprise.setId(numEnterprises);

        Element newEnterprise = input.createElement("node");
        newEnterprise.setAttribute("id", String.valueOf(numEnterprises));
        newEnterprise.setAttribute("type", "ENTERPRISE");

        Element propertyName = input.createElement("property");
        propertyName.setAttribute("name", "enterprisename");
        propertyName.setAttribute("value", enterprise.getEnterpriseName());

        Element propertyComment = input.createElement("property");
        propertyComment.setAttribute("name", "comment");
        propertyComment.setAttribute("value", enterprise.getComment());

        rootNode.appendChild(newEnterprise);
        newEnterprise.appendChild(propertyName);
        newEnterprise.appendChild(propertyComment);


        return true;
    }

    public List<Enterprise> getAllEnterprises (Document input) {

        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<Enterprise> list = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++){
            Enterprise enterprise = getEnterprise(nodeList.item(i));

            if (enterprise != null) {
                list.add(enterprise);
            }
        }
        return list;
    }

    public boolean addEnterprise (Document input, Enterprise enterprise, List<Subsystem> subsystemList) {

        boolean success = addEnterprise(input, enterprise);

        if (!success) {
            return false;
        }

        for (Subsystem item : subsystemList) {
            addEdgeBetweenEnterAndSub(input, enterprise, item);
        }

        return true;
    }

    public List<Subsystem> getSubsystemsByEnterprise (Document input, Enterprise enterprise) {

        NodeList nodeList = input.getDocumentElement().getChildNodes();
        List<String> list = new ArrayList<>();
        List<Subsystem> subsystemList = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++){
            String toId = getSubIdByEnterprise(nodeList.item(i), enterprise);

            if (toId != null) {
                list.add(toId);
            }
        }

        for (int i = 0; i < nodeList.getLength(); i++){
            Subsystem subsystem = getSubsystemByIds(nodeList.item(i), list);

            if (subsystem != null) {
                subsystemList.add(subsystem);
            }
        }

        return subsystemList;
    }



    private Subsystem getSubsystemByIds(Node node, List<String> listIds) {
        String name = node.getNodeName();

        Subsystem subsystem = null;
        if (name.equals("node")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String id = nodeMap.getNamedItem("id").getNodeValue();
                if (type.equals("SUBSYSTEM")) {
                    for (String strId : listIds) {
                        if (strId.equals(id)){
                            subsystem = new Subsystem(Integer.parseInt(id));
                            NodeList nodeList = node.getChildNodes();

                            for (int i = 0; i < nodeList.getLength(); i++){
                                subsystemProperty(nodeList.item(i), subsystem);
                            }
                            break;
                        }
                    }

                }
            }
        }

        return subsystem;
    }

    private void subsystemProperty(Node node, Subsystem subsystem) {
        String nameNod = node.getNodeName();

        if (nameNod.equals("property")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String name = nodeMap.getNamedItem("name").getNodeValue();
                String value = nodeMap.getNamedItem("value").getNodeValue();
                if (name.equals("subname")) {
                    subsystem.setSubsystemName(value);
                }

                if (name.equals("comment")) {
                    subsystem.setComment(value);
                }
            }
        }
    }

    private String getSubIdByEnterprise(Node node, Enterprise enterprise) {
        String name = node.getNodeName();
        String enterId = String.valueOf(enterprise.getId());

        if (name.equals("edge")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String toid = nodeMap.getNamedItem("toid").getNodeValue();
                String fromid = nodeMap.getNamedItem("fromid").getNodeValue();
                if (type.equals("ENTERPRISE_SUBSYSTEM") && fromid.equals(enterId)) {
                    return toid;
                }
            }
        }

        return null;
    }



    private Enterprise getEnterprise(Node node) {
        String name = node.getNodeName();

        Enterprise enterprise = null;
        if (name.equals("node")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String type = nodeMap.getNamedItem("type").getNodeValue();
                String id = nodeMap.getNamedItem("id").getNodeValue();
                if (type.equals("ENTERPRISE")) {
                    enterprise = new Enterprise(Integer.parseInt(id));
                    NodeList nodeList = node.getChildNodes();

                    for (int i = 0; i < nodeList.getLength(); i++){
                        enterpriseProperty(nodeList.item(i), enterprise);
                    }
                }
            }
        }

        return enterprise;
    }



    private void enterpriseProperty(Node node, Enterprise enterprise) {
        String nameNod = node.getNodeName();

        if (nameNod.equals("property")) {
            NamedNodeMap nodeMap = node.getAttributes();
            if(nodeMap != null) {
                String name = nodeMap.getNamedItem("name").getNodeValue();
                String value = nodeMap.getNamedItem("value").getNodeValue();
                if (name.equals("enterprisename")) {
                    enterprise.setEnterpriseName(value);
                }

                if (name.equals("comment")) {
                    enterprise.setComment(value);
                }
            }
        }
    }

    private Node getEnterpriseNodeById(Document input, String searchedId) {
        NodeList nodeList = input.getElementsByTagName("node");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String type = nodeMap.getNamedItem("type").getNodeValue();
            String id = nodeMap.getNamedItem("id").getNodeValue();
            if (type.equals("ENTERPRISE") && searchedId.equals(id)) {
                return item;
            }
        }
        return null;
    }

    private List<Node> getEdgeBetweenEnterSub (Document input, Enterprise enterprise) {
        NodeList nodeList = input.getElementsByTagName("edge");
        String enterId = String.valueOf(enterprise.getId());
        List<Node> listNodes = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            NamedNodeMap nodeMap = item.getAttributes();
            String fromId = nodeMap.getNamedItem("fromid").getNodeValue();
            if (fromId.equals(enterId)) {
                listNodes.add(item);
            }
        }

        return listNodes;
    }
}
