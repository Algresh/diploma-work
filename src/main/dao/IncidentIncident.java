package main.dao;

public class IncidentIncident extends IncidentLink{

    private boolean issuedby;
    private boolean leadto;
    private String comment;

    public IncidentIncident(int fromId, boolean issuedby, boolean leadto, String comment) {
        this.fromId = fromId;
        this.issuedby = issuedby;
        this.leadto = leadto;
        this.comment = comment;
    }

    public boolean issuedby() {
        return issuedby;
    }

    public void setIssuedby(boolean issuedby) {
        this.issuedby = issuedby;
    }

    public boolean isLeadto() {
        return leadto;
    }

    public void setLeadto(boolean leadto) {
        this.leadto = leadto;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
