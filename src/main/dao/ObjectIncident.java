package main.dao;

public class ObjectIncident extends IncidentLink{
    private boolean issuedby;
    private boolean affect;
    private String comment;

    public ObjectIncident(int fromId, boolean issuedby, boolean affect, String comment) {
        this.fromId = fromId;
        this.issuedby = issuedby;
        this.affect = affect;
        this.comment = comment;
    }

    public boolean issuedby() {
        return issuedby;
    }

    public void setIssuedby(boolean issuedby) {
        this.issuedby = issuedby;
    }

    public boolean isAffect() {
        return affect;
    }

    public void setAffect(boolean affect) {
        this.affect = affect;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
