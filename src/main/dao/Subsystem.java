package main.dao;


public class Subsystem {

    private int id;
    private String subsystemName;
    private String comment;

    public Subsystem(int id, String subsystemName, String comment) {
        this.id = id;
        this.subsystemName = subsystemName;
        this.comment = comment;
    }

    public Subsystem(String subsystemName, String comment) {
        this.subsystemName = subsystemName;
        this.comment = comment;
    }

    public Subsystem(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubsystemName() {
        return subsystemName;
    }

    public void setSubsystemName(String subsystemName) {
        this.subsystemName = subsystemName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

//    @Override
//    public String toString() {
//        return "Subsystem{" +
//                "id=" + id +
//                ", subsystemName='" + subsystemName + '\'' +
//                ", comment='" + comment + '\'' +
//                '}';
//    }

    @Override
    public String toString() {
        return id + ") " + subsystemName;
    }
}
