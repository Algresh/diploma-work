package main.dao;

public class Component {

    private int id;
    private String compName;
    private String vendor;
    private String comment;

    public Component(int id) {
        this.id = id;
    }

    public Component(int id, String compName, String vendor, String comment) {
        this.id = id;
        this.compName = compName;
        this.vendor = vendor;
        this.comment = comment;
    }

    public Component( String compName, String vendor, String comment) {
        this.compName = compName;
        this.vendor = vendor;
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

//    @Override
//    public String toString() {
//        return "Component{" +
//                "id=" + id +
//                ", compName='" + compName + '\'' +
//                ", vendor='" + vendor + '\'' +
//                ", comment='" + comment + '\'' +
//                '}';
//    }

    @Override
    public String toString() {
        return  id + ") " + compName;
    }
}
