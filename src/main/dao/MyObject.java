package main.dao;

public class MyObject {

    private int id;
    private String objname;
    private String belongsto;


    public MyObject(int id) {
        this.id = id;
    }

    public MyObject(String objname, String belongsto) {
        this.objname = objname;
        this.belongsto = belongsto;
    }

    public MyObject(int id, String objname, String belongsto) {
        this.id = id;
        this.objname = objname;
        this.belongsto = belongsto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getObjname() {
        return objname;
    }

    public void setObjname(String objname) {
        this.objname = objname;
    }

    public String getBelongsto() {
        return belongsto;
    }

    public void setBelongsto(String belongsto) {
        this.belongsto = belongsto;
    }

//    @Override
//    public String toString() {
//        return "MyObject{" +
//                "id=" + id +
//                ", objname='" + objname + '\'' +
//                ", belongsto='" + belongsto + '\'' +
//                '}';
//    }


    @Override
    public String toString() {
        return id + ") " + objname;
    }
}
