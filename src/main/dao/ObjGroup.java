package main.dao;


public class ObjGroup {
    private int id;
    private String objgname;
    private String belongsto;

    public ObjGroup(int id, String objgname, String belongsto) {
        this.id = id;
        this.objgname = objgname;
        this.belongsto = belongsto;
    }

    public ObjGroup(int id) {
        this.id = id;
    }

    public ObjGroup(String objgname, String belongsto) {
        this.objgname = objgname;
        this.belongsto = belongsto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getObjgname() {
        return objgname;
    }

    public void setObjgname(String objgname) {
        this.objgname = objgname;
    }

    public String getBelongsto() {
        return belongsto;
    }

    public void setBelongsto(String belongsto) {
        this.belongsto = belongsto;
    }

//    @Override
//    public String toString() {
//        return "ObjGroup{" +
//                "id=" + id +
//                ", objgname='" + objgname + '\'' +
//                ", belongsto='" + belongsto + '\'' +
//                '}';
//    }


    @Override
    public String toString() {
        return id + ") " + objgname;
    }
}
