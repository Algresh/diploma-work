package main.dao;

public class Enterprise {

    private int id;
    private String enterpriseName;
    private String comment;

    public Enterprise(int id, String enterpriseName, String comment) {
        this.id = id;
        this.enterpriseName = enterpriseName;
        this.comment = comment;
    }

    public Enterprise( String enterpriseName, String comment) {
        this.enterpriseName = enterpriseName;
        this.comment = comment;
    }

    public Enterprise(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

//    @Override
//    public String toString() {
//        return "Enterprise{" +
//                "id=" + id +
//                ", enterpriseName='" + enterpriseName + '\'' +
//                ", comment='" + comment + '\'' +
//                '}';
//    }

    @Override
    public String toString() {
        return id + ") " + enterpriseName;
    }
}
