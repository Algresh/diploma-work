package main.dao;

public class Incident {
    private int id;
    private String code;
    private String message;
    private String severity;
    private String type;
    private String event;
    private String action;
    private int osi;
    private String comment;


    public Incident(int id, String code, String message, String severity, String type, String event, String action, int osi, String comment) {
        this.id = id;
        this.code = code;
        this.message = message;
        this.severity = severity;
        this.type = type;
        this.event = event;
        this.action = action;
        this.osi = osi;
        this.comment = comment;
    }

    public Incident(String code, String message, String severity, String type, String event, String action, int osi, String comment) {
        this.code = code;
        this.message = message;
        this.severity = severity;
        this.type = type;
        this.event = event;
        this.action = action;
        this.osi = osi;
        this.comment = comment;
    }

    public Incident(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getOsi() {
        return osi;
    }

    public void setOsi(int osi) {
        this.osi = osi;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

//    @Override
//    public String toString() {
//        return "Incident{" +
//                "id=" + id +
//                ", code='" + code + '\'' +
//                ", message='" + message + '\'' +
//                ", severity='" + severity + '\'' +
//                ", type='" + type + '\'' +
//                ", event='" + event + '\'' +
//                ", action='" + action + '\'' +
//                ", osi=" + osi +
//                ", comment='" + comment + '\'' +
//                '}';
//    }


    @Override
    public String toString() {
        return id + ") " + code;
    }
}
